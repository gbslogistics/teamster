<?php

namespace GbsLogistics\Teamster\BackendBundle;
use Doctrine\ORM\EntityRepository;
use GbsLogistics\SdeEntityBundle\Entity\RamInstallationTypeContents;

/**
 * Loads ramActivities from the SDE. Since this table is relatively small, we
 * can just load the entire thing into memory if the class is actually needed.
 *
 * Class OutpostRamActivityDictionary
 * @package GbsLogistics\Teamster\BackendBundle
 */
class OutpostRamActivityDictionary
{
    /** @var boolean */
    private $initialized;

    /** @var array */
    private $dictionary = [ ];

    /** @var EntityRepository */
    private $repository;

    function __construct(EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getRamActivities($outpostTypeId)
    {
        $this->initialize();

        if (isset($this->dictionary[$outpostTypeId])) {
            return $this->dictionary[$outpostTypeId];
        }

        return [];
    }

    /**
     * Loads ramActivities for all outposts into memory. This won't get called
     * until someone asks for ramActivities, so there's little risk of the call
     * occurring outside of the cron jobs.
     */
    private function initialize()
    {
        if (!$this->initialized) {
            /** @var RamInstallationTypeContents $ramInstallation */
            foreach ($this->repository->findAll() as $ramInstallation) {
                $typeId = $ramInstallation->getInstallationTypeID();

                if (!isset($this->dictionary[$typeId])) {
                    $this->dictionary[$typeId] = [];
                }

                $activityID = $ramInstallation->getAssemblyLineType()->getActivityID();
                $this->dictionary[$typeId][] = $activityID;
            }

            foreach ($this->dictionary as $typeId => $activities) {
                $this->dictionary[$typeId] = array_values(array_unique($activities));
            }

            $this->initialized = true;
        }
    }
}
