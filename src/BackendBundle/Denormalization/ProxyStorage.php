<?php

namespace GbsLogistics\Teamster\BackendBundle\Denormalization;
use GbsLogistics\Teamster\DocumentBundle\Model\Proxy;


/**
 * Stores denormalization proxies for the Processor.
 *
 * Class ProxyStorage
 * @package GbsLogistics\Teamster\BackendBundle\Denormalization
 */
class ProxyStorage
{
    /** @var array */
    private $proxyList = [];

    /** @var array */
    private $unpackedProxies = [];

    /** @var bool */
    private $unpacked = false;

    public function addProxy(Proxy $proxy)
    {
        $routineClass = $proxy->getRoutineClass();
        if (!isset($this->proxyList[$routineClass])) {
            $this->proxyList[$routineClass] = [];
        }
        $this->proxyList[$routineClass][] = $proxy;
    }

    /**
     * @return \Generator
     */
    public function getRoutineClasses()
    {
        return array_keys($this->proxyList);
    }

    /**
     * @param string $routineClass
     * @return array
     */
    public function getDiscriminators($routineClass)
    {
        $this->unpackProxies();

        if (isset($this->unpackedProxies[$routineClass])) {
            return array_keys($this->unpackedProxies[$routineClass]);
        }

        return [];
    }

    /**
     * Returns all the proxies for the given class and discriminator.
     *
     * @param string $routineClass
     * @param mixed $discriminator
     * @return \Generator
     */
    public function getProxies($routineClass, $discriminator)
    {
        $this->unpackProxies();

        if (isset($this->unpackedProxies[$routineClass])
            && isset($this->unpackedProxies[$routineClass][$discriminator])
        ) {
            foreach ($this->unpackedProxies[$routineClass][$discriminator] as $proxy) {
                yield $proxy;
            }
        }
    }

    /**
     * Iterates through all the proxies, resolving their discriminators and
     * unpacking them into a map to be resolved later.
     */
    private function unpackProxies()
    {
        if (!$this->unpacked) {
            foreach ($this->proxyList as $routineClass => $proxyList) {
                /** @var Proxy $proxy */
                foreach ($proxyList as $proxy) {
                    $discriminatorCallback = $proxy->getDiscriminatorCallback();
                    $discriminator = $discriminatorCallback();

                    if (null === $discriminator || '' === $discriminator) {
                        throw new \RuntimeException(sprintf(
                            'Discriminator returned from proxy utilizing routine "%s" was null or an empty string.' .
                            ' Did you forget to return a value in the discriminator closure?',
                            $routineClass
                        ));
                    }

                    if (!isset($this->unpackedProxies[$routineClass])) {
                        $this->unpackedProxies[$routineClass] = [];
                    }

                    if (!isset($this->unpackedProxies[$routineClass][$discriminator])) {
                        $this->unpackedProxies[$routineClass][$discriminator] = [];
                    }

                    $this->unpackedProxies[$routineClass][$discriminator][] = $proxy;
                }
            }

            $this->unpacked = true;
        }
    }
}
