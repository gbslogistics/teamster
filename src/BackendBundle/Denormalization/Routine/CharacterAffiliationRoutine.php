<?php

namespace GbsLogistics\Teamster\BackendBundle\Denormalization\Routine;


use GbsLogistics\Teamster\DocumentBundle\Document\CharacterAffiliation;
use GbsLogistics\Teamster\DocumentBundle\PhealMapper;
use Pheal\Pheal;

class CharacterAffiliationRoutine implements DenormalizationRoutineInterface
{
    const MAX_IDS_PER_CALL = 50;

    /** @var Pheal */
    private $phealEveScope;

    /** @var PhealMapper */
    private $phealMapper;

    function __construct(Pheal $pheal = null, PhealMapper $phealMapper = null)
    {
        $this->phealEveScope = $pheal ?: new Pheal(null, null, 'eve');
        $this->phealMapper = $phealMapper ?: new PhealMapper();
    }

    /**
     * Performs the expensive lookup on the discriminators, returning objects to
     * be used for denormalization.
     *
     * @param array $discriminators
     * @return \Generator
     */
    public function performDenormalizationLookup(array $discriminators)
    {
        $callCount = ceil(count($discriminators) / self::MAX_IDS_PER_CALL);
        for ($i = 0; $i < $callCount; $i++) {
            $discriminatorSlice = array_slice(
                $discriminators,
                $i * self::MAX_IDS_PER_CALL,
                self::MAX_IDS_PER_CALL
            );

            if (count($discriminatorSlice) > 0) {
                /** @noinspection PhpUndefinedMethodInspection */
                $results = $this->phealEveScope->CharacterAffiliation(['ids' => implode(',', $discriminatorSlice)]);

                foreach ($results->characters as $result) {
                    $characterAffiliation = new CharacterAffiliation();
                    $this->phealMapper->map($result, $characterAffiliation);

                    yield $characterAffiliation;
                }
            }
        }
    }

    /**
     * @param mixed $result
     * @return mixed
     */
    public function extractDiscriminatorFromResult($result)
    {
        /** @var CharacterAffiliation $result */
        return $result->getCharacterID();
    }
}