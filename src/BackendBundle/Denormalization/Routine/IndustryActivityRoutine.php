<?php

namespace GbsLogistics\Teamster\BackendBundle\Denormalization\Routine;


use Doctrine\ORM\EntityRepository;
use GbsLogistics\SdeEntityBundle\Entity\RamActivity;
use GbsLogistics\Teamster\BackendBundle\Model\IndustryActivityRoutineResult;

class IndustryActivityRoutine implements DenormalizationRoutineInterface
{
    /** @var EntityRepository */
    private $entityRepository;

    function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    /**
     * Performs the expensive lookup on the discriminators, returning objects to
     * be used for denormalization.
     *
     * REMINDER: When implementing a new denormalized field, make sure to go the
     * BCC auto-mapper for the object in question and ensure skipNull is set.
     *
     * @param array $discriminators
     * @return \Generator
     */
    public function performDenormalizationLookup(array $discriminators)
    {
        /** @var RamActivity $activity */
        foreach ($this->entityRepository->findBy(['activityID' => $discriminators]) as $activity) {
            yield $activity;
        }
    }

    /**
     * @param mixed $result
     * @return mixed
     */
    public function extractDiscriminatorFromResult($result)
    {
        if (!($result instanceof RamActivity)) {
            throw new \RuntimeException("Expected result to be an instance of RamActivity.");
        }

        /** @var RamActivity $result */
        return $result->getActivityID();
    }
}