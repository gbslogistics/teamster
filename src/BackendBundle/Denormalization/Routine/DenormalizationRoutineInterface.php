<?php

namespace GbsLogistics\Teamster\BackendBundle\Denormalization\Routine;

interface DenormalizationRoutineInterface
{
    /**
     * Performs the expensive lookup on the discriminators, returning objects to
     * be used for denormalization.
     *
     * REMINDER: When implementing a new denormalized field, make sure to go the
     * BCC auto-mapper for the object in question and ensure skipNull is set.
     *
     * @param array $discriminators
     * @return \Generator
     */
    public function performDenormalizationLookup(array $discriminators);

    /**
     * @param mixed $result
     * @return mixed
     */
    public function extractDiscriminatorFromResult($result);
}