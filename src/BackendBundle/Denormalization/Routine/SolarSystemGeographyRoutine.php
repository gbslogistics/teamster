<?php

namespace GbsLogistics\Teamster\BackendBundle\Denormalization\Routine;

use Doctrine\ORM\EntityRepository;
use GbsLogistics\SdeEntityBundle\Entity\MapSolarSystem;
use GbsLogistics\SdeEntityBundle\Entity\RamAssemblyLineStation;
use GbsLogistics\SdeEntityBundle\Entity\Repository\RamAssemblyLineStationRepository;
use GbsLogistics\Teamster\BackendBundle\OutpostRamActivityDictionary;
use GbsLogistics\Teamster\DocumentBundle\Document\ConquerableStation;
use GbsLogistics\Teamster\DocumentBundle\Document\Repository\ConquerableStationRepository;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemGeography;

/**
 * TODO Write tests
 *
 * Class SolarSystemGeographyRoutine
 * @package GbsLogistics\Teamster\BackendBundle\Denormalization\Routine
 */
class SolarSystemGeographyRoutine implements DenormalizationRoutineInterface
{
    const MAX_LOOKUPS_PER_ITERATION = 999;

    /** @var EntityRepository */
    private $mapSolarSystemRepository;

    /** @var RamAssemblyLineStationRepository */
    private $ramAssemblyLineStationRepository;

    /** @var ConquerableStationRepository */
    private $conquerableStationRepository;

    /** @var OutpostRamActivityDictionary */
    private $outpostRamActivityDictionary;

    function __construct(
        EntityRepository $mapSolarSystemRepository,
        RamAssemblyLineStationRepository $ramAssemblyLineStationRepository,
        ConquerableStationRepository $conquerableStationRepository,
        OutpostRamActivityDictionary $outpostRamActivityDictionary
    ) {
        $this->mapSolarSystemRepository = $mapSolarSystemRepository;
        $this->ramAssemblyLineStationRepository = $ramAssemblyLineStationRepository;
        $this->conquerableStationRepository = $conquerableStationRepository;
        $this->outpostRamActivityDictionary = $outpostRamActivityDictionary;
    }

    /**
     * @param array $discriminators
     * @return \Generator
     */
    public function performDenormalizationLookup(array $discriminators)
    {
        $solarSystems = $this->getSolarSystems($discriminators);
        $conquerableStations = $this->conquerableStationRepository->fetchBySolarSystemIdsIndexed($discriminators);
        $assemblyLines = $this->getRamAssemblyLineStations($discriminators);

        /** @var MapSolarSystem $system */
        foreach ($solarSystems as $system) {
            $solarSystemId = $system->getSolarSystemID();

            $wormholeClass = $system->getRegion()->getWormholeClass()->getWormholeClassID();
            if ($wormholeClass > 6) {
                $wormholeClass = null;
            }

            $geography = new SolarSystemGeography();
            $geography->setConstellationId($system->getConstellationID());
            $geography->setRegionId($system->getRegionID());
            $geography->setConstellationName($system->getConstellation()->getConstellationName());
            $geography->setRegionName($system->getRegion()->getRegionName());
            $geography->setWormholeClass($wormholeClass);
            $geography->setDiscriminator($solarSystemId);
            $geography->setFactionId($system->getFactionID());
            $geography->setSecurity($system->getSecurity());

            if (isset($assemblyLines[$solarSystemId])) {
                $facilityTypes = array_map(function (RamAssemblyLineStation $ramAssemblyLineStation) {
                    return $ramAssemblyLineStation->getAssemblyLineType()->getActivityID();
                }, $assemblyLines[$solarSystemId]);
                $facilityTypes = array_unique($facilityTypes);
                $geography->setFacilityTypes($facilityTypes);
            } elseif (isset($conquerableStations[$solarSystemId])) {
                /** @var ConquerableStation $station */
                $station = $conquerableStations[$solarSystemId];
                $geography->setFacilityTypes($this->outpostRamActivityDictionary
                    ->getRamActivities($station->getStationTypeID()));
            }

            yield $geography;
        }
    }

    public function extractDiscriminatorFromResult($result)
    {
        if (!($result instanceof SolarSystemGeography)) {
            throw new \RuntimeException('Expected to extract from an object of type SolarSystemGeography.');
        }

        /** @var $result SolarSystemGeography */
        return $result->getDiscriminator();
    }

    /**
     * @param array $discriminators
     * @return \Generator
     */
    private function getSolarSystems(array $discriminators)
    {
        $totalIterations = ceil(count($discriminators) / self::MAX_LOOKUPS_PER_ITERATION);

        for ($i = 0; $i < $totalIterations; $i++) {
            $discriminatorSlice = array_slice(
                $discriminators,
                $i * self::MAX_LOOKUPS_PER_ITERATION,
                self::MAX_LOOKUPS_PER_ITERATION
            );

            $solarSystems = $this->mapSolarSystemRepository->findBy(['solarSystemID' => $discriminatorSlice]);

            foreach ($solarSystems as $solarSystem) {
                yield $solarSystem;
            }
        }
    }

    /**
     * @param array $discriminators
     * @return array
     */
    private function getRamAssemblyLineStations(array $discriminators)
    {
        $totalIterations = ceil(count($discriminators) / self::MAX_LOOKUPS_PER_ITERATION);
        $mergedAssemblyLines = [];

        for ($i = 0; $i < $totalIterations; $i++) {
            $discriminatorSlice = array_slice(
                $discriminators,
                $i * self::MAX_LOOKUPS_PER_ITERATION,
                self::MAX_LOOKUPS_PER_ITERATION
            );

            $assemblyLines = $this->ramAssemblyLineStationRepository->findBy([
                'solarSystemID' => $discriminatorSlice,
            ]);

            if (null !== $assemblyLines) {
                /** @var RamAssemblyLineStation $line */
                foreach ($assemblyLines as $line) {
                    $solarSystemId = $line->getSolarSystemID();
                    if (!isset($mergedAssemblyLines[$solarSystemId])) {
                        $mergedAssemblyLines[$solarSystemId] = [];
                    }
                    $mergedAssemblyLines[$solarSystemId][] = $line;
                }
            }
        }

        return $mergedAssemblyLines;
    }
}
