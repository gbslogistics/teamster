<?php

namespace GbsLogistics\Teamster\BackendBundle\Denormalization\Routine;


use GbsLogistics\Doramad\Domain\SpecializationTypeEnum;
use GbsLogistics\Teamster\BackendBundle\Model\WorkerBonusTypeRoutineResult;

class WorkerBonusTypeRoutine implements DenormalizationRoutineInterface
{
    /**
     * Performs the expensive lookup on the discriminators, returning objects to
     * be used for denormalization.
     *
     * @param array $discriminators
     * @return \Generator
     */
    public function performDenormalizationLookup(array $discriminators)
    {
        foreach ($discriminators as $discriminator) {
            $bonusType = null;
            if ($discriminator === SpecializationTypeEnum::ME) {
                $bonusType = 'ME';
            } elseif ($discriminator === SpecializationTypeEnum::TE) {
                $bonusType = 'TE';
            }

            yield new WorkerBonusTypeRoutineResult($bonusType, $discriminator);
        }
    }

    /**
     * @param mixed $result
     * @return mixed
     */
    public function extractDiscriminatorFromResult($result)
    {
        /** @var $result WorkerBonusTypeRoutineResult */
        if (!($result instanceof WorkerBonusTypeRoutineResult)) {
            throw new \InvalidArgumentException('Expected result to be instance of WorkerBonusTypeRoutineResult.');
        }

        return $result->getDiscriminator();
    }
}