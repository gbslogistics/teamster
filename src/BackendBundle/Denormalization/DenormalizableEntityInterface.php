<?php
namespace GbsLogistics\Teamster\BackendBundle\Denormalization;

interface DenormalizableEntityInterface
{
    /**
     * Requests a list of denormalization proxies from the implementing entity.
     *
     * @return array<ProxyInterface>
     */
    public function getDenormalizationProxies();
}
