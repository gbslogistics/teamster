<?php

namespace GbsLogistics\Teamster\BackendBundle\Denormalization;


use GbsLogistics\Teamster\BackendBundle\Denormalization\Routine\DenormalizationRoutineInterface;
use GbsLogistics\Teamster\DocumentBundle\Model\Proxy;

class Processor
{
    /** @var ProxyStorage */
    private $proxyStorage;

    /** @var \SplObjectStorage */
    private $objectStorage;

    /** @var callback */
    private $postDenormalizationCallback = null;

    /** @var array */
    private $routines = [];

    function __construct()
    {
        $this->objectStorage = new \SplObjectStorage();
    }

    /**
     * Adds an object to be denormalized later.
     *
     * @param DenormalizableEntityInterface $object
     * @return void
     */
    public function queueObject(DenormalizableEntityInterface $object)
    {
        $proxies = $object->getDenormalizationProxies();
        /** @var Proxy $proxy */
        foreach ($proxies as $proxy) {
            // Skip things that have already been denormalized
            if (!($proxy instanceof Proxy)) {
                continue;
            }

            $this->proxyStorage->addProxy($proxy);
        }

        if (null !== $this->postDenormalizationCallback) {
            $this->objectStorage->attach($object);
        }
    }

    public function processQueue()
    {
        foreach ($this->proxyStorage->getRoutineClasses() as $routineClass) {
            if (!isset($this->routines[$routineClass])) {
                throw new \RuntimeException(sprintf(
                    'Denormalization routine "%s" was not registered.',
                    $routineClass
                ));
            }

            /** @var DenormalizationRoutineInterface $routine */
            $routine = $this->routines[$routineClass];
            $discriminators = $this->proxyStorage->getDiscriminators($routineClass);

            if (count($discriminators) > 0) {
                foreach ($routine->performDenormalizationLookup($discriminators) as $result) {
                    $discriminator = $routine->extractDiscriminatorFromResult($result);

                    /** @var Proxy $proxy */
                    foreach ($this->proxyStorage->getProxies($routineClass, $discriminator) as $proxy) {
                        $callback = $proxy->getDenormalizationCallback();
                        $callback($result);
                    }
                }
            }
        }

        if (null !== $callback = $this->postDenormalizationCallback) {
            foreach ($this->objectStorage as $object) {
                $callback($object);
            }
        }
    }

    public function registerRoutine(DenormalizationRoutineInterface $routine)
    {
        $this->routines[get_class($routine)] = $routine;
    }

    /**
     * NOTE: You must set this value BEFORE adding items to the queue, if you
     * wish to use this feature. Otherwise, objects being denormalized will not
     * be stored.
     *
     * @param callable $postDenormalizationCallback
     */
    public function setPostDenormalizationCallback($postDenormalizationCallback)
    {
        $this->postDenormalizationCallback = $postDenormalizationCallback;
    }

    /**
     * @param ProxyStorage $proxyStorage
     */
    public function setProxyStorage($proxyStorage)
    {
        $this->proxyStorage = $proxyStorage;
    }
}
