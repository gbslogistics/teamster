<?php

namespace GbsLogistics\Teamster\BackendBundle\Factory;
use Doctrine\ODM\MongoDB\DocumentManager;
use GbsLogistics\Teamster\DocumentBundle\Document\Batch;


/**
 * Creates a new batch and returns it.
 *
 * @author Querns <querns@gbs.io>
 */
class BatchFactory
{
    const CHARTERED_TEAM = 'chartered-team';
    const AUCTION_TEAM = 'auction-team';

    public function createCharteredTeamBatch()
    {
        return $this->createBatch(self::CHARTERED_TEAM);
    }

    public function createAuctionTeamBatch()
    {
        return $this->createBatch(self::AUCTION_TEAM);
    }

    /**
     * @param $batchType
     * @return Batch
     */
    private function createBatch($batchType)
    {
        $batch = new Batch();
        $batch->setBatchNumber(gmdate('U'));
        $batch->setType($batchType);

        return $batch;
    }
} 
