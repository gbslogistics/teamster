<?php

namespace GbsLogistics\Teamster\BackendBundle\Task;

use Doctrine\ODM\MongoDB\DocumentManager;


/**
 * Contains common routines for tasks.
 *
 * @author Querns <querns@gbs.io>
 */
abstract class BaseTask
{
    /** @var DocumentManager */
    protected $dm;

    /**
     * @param string $className
     * @param object $object
     */
    protected function verifyIsInstanceOf($className, $object)
    {
        if (!is_object($object)) {
            throw new \RuntimeException(sprintf(
                'Unexpected type "%s" in array retrieved from CREST client.',
                gettype($object)
            ));
        } elseif (!is_a($object, $className)) {
            throw new \RuntimeException(sprintf(
                'Unexpected object of type "%s" in array retrieved from CREST client.',
                get_class($object)
            ));
        }
    }
}