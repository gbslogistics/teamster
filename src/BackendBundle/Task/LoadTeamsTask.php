<?php

namespace GbsLogistics\Teamster\BackendBundle\Task;

use Doctrine\ODM\MongoDB\DocumentManager;
use GbsLogistics\Doramad\Domain\IndustryTeam as DomainTeam;
use GbsLogistics\Teamster\BackendBundle\CrestClient;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Processor;
use GbsLogistics\Teamster\BackendBundle\Factory\BatchFactory;
use GbsLogistics\Teamster\DocumentBundle\Document\Batch;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustryTeam;
use GbsLogistics\Teamster\DocumentBundle\DomainMapper;


/**
 * Loads chartered teams from CREST and persists them.
 *
 * @author Querns <querns@gbs.io>
 * @TODO Add denormalization proxy to solar system of team if it changes
 */
class LoadTeamsTask extends BaseTask
{
    const CHARTERED_TEAMS_ENDPOINT_PATH = '/industry/teams/';
    const AUCTION_TEAMS_ENDPOINT_PATH = '/industry/teams/auction/';

    /** @var CrestClient */
    private $crestClient;

    /** @var BatchFactory */
    private $batchFactory;

    /** @var DomainMapper */
    private $mapper;

    /** @var \Doctrine\ODM\MongoDB\DocumentRepository */
    private $industryTeamRepository;

    /** @var Processor */
    private $denormalizationProcessor;

    function __construct(
        CrestClient $crestClient,
        DocumentManager $dm,
        BatchFactory $batchFactory,
        DomainMapper $mapper,
        Processor $processor
    )
    {
        $this->crestClient = $crestClient;
        $this->dm = $dm;
        $this->batchFactory = $batchFactory;
        $this->mapper = $mapper;
        $this->denormalizationProcessor = $processor;

        $processor->setPostDenormalizationCallback(function (IndustryTeam $team) {
            $this->dm->persist($team);
        });

        $this->industryTeamRepository = $this->dm->getRepository(IndustryTeam::class);
    }

    public function loadCharteredTeams()
    {
        $batch = $this->batchFactory->createCharteredTeamBatch();
        $this->dm->persist($batch);
        $this->loadTeams(self::CHARTERED_TEAMS_ENDPOINT_PATH, $batch);
        $this->dm->flush();
    }

    public function loadAuctionTeams()
    {
        $batch = $this->batchFactory->createAuctionTeamBatch();
        $this->dm->persist($batch);
        $this->loadTeams(self::AUCTION_TEAMS_ENDPOINT_PATH, $batch);
        $this->dm->flush();
    }

    /**
     * TODO: Refactor; terribly inefficient
     * @param string $endpoint
     * @param Batch $batch
     */
    private function loadTeams($endpoint, Batch $batch)
    {
        /** @var array $domainTeams */
        $domainTeams = $this->crestClient->loadCrestResource($endpoint);

        if (!is_array($domainTeams)) {
            if ($domainTeams instanceof DomainTeam) {
                $domainTeams = [$domainTeams];
            } else {
                /** @var object $domainTeams */
                $this->verifyIsInstanceOf(DomainTeam::class, $domainTeams);
            }
        }

        /** @var DomainTeam $domainTeam */
        foreach ($domainTeams as $domainTeam) {
            $this->verifyIsInstanceOf(DomainTeam::class, $domainTeam);

            $existingTeam = $this->industryTeamRepository->findOneBy([
                'teamId' => $domainTeam->getTeamId(),
            ]);

            if (null === $existingTeam) {
                // New team
                $documentTeam = $this->convertDomainTeamToDocumentTeam($domainTeam);
                $documentTeam->setBatchNumber($batch->getBatchNumber());
                $this->denormalizationProcessor->queueObject($documentTeam);
            } else {
                $this->mapper->map($domainTeam, $existingTeam);
                $existingTeam->setBatchNumber($batch->getBatchNumber());
                $this->denormalizationProcessor->queueObject($existingTeam);
            }
        }

        $this->denormalizationProcessor->processQueue();
    }

    /**
     * @param DomainTeam $domainTeam
     * @return IndustryTeam
     */
    private function convertDomainTeamToDocumentTeam(DomainTeam $domainTeam)
    {
        $documentTeam = new IndustryTeam();
        $this->mapper->map($domainTeam, $documentTeam);

        return $documentTeam;
    }
}

