<?php

namespace GbsLogistics\Teamster\BackendBundle\Task;


use BCC\AutoMapperBundle\Mapper\Mapper;
use Doctrine\ODM\MongoDB\DocumentManager;
use GbsLogistics\Teamster\DocumentBundle\Document\ConquerableStation;
use GbsLogistics\Teamster\DocumentBundle\MongoCollectionHelper;
use GbsLogistics\Teamster\DocumentBundle\PhealMapper;
use Pheal\Core\RowSetRow;
use Pheal\Pheal;

class LoadConquerableStationsTask
{
    /** @var Pheal */
    private $pheal;

    /** @var PhealMapper */
    private $mapper;

    /** @var DocumentManager */
    private $dm;

    /** @var MongoCollectionHelper */
    private $collectionHelper;

    function __construct(DocumentManager $dm, MongoCollectionHelper $collectionHelper)
    {
        $this->mapper = new PhealMapper();
        $this->pheal = new Pheal();
        $this->dm = $dm;
        $this->collectionHelper = $collectionHelper;
    }

    public function loadConquerableStations()
    {
        $results = $this->pheal->eveScope->ConquerableStationList();

        /** @var RowSetRow $result */
        foreach ($results->outposts as $result) {
            $station = new ConquerableStation();
            $this->mapper->map($result, $station);

            $this->dm->persist($station);
        }

        $this->collectionHelper->purgeMongoCollection(ConquerableStation::class);
        $this->dm->flush();
    }
}
