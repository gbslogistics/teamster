<?php

namespace GbsLogistics\Teamster\BackendBundle\Task;

use Doctrine\ODM\MongoDB\DocumentManager;
use GbsLogistics\Doramad\Domain\IndustrySpeciality as DomainSpeciality;
use GbsLogistics\Teamster\BackendBundle\CrestClient;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustrySpeciality;
use GbsLogistics\Teamster\DocumentBundle\DomainMapper;
use GbsLogistics\Teamster\DocumentBundle\MongoCollectionHelper;
use GbsLogistics\Teamster\DocumentBundle\Tests\Database\IndustrySpecialityTest;


/**
 * Loads industry specialities from CREST. Shouldn't need to be done very often.
 *
 * @author Querns <querns@gbs.io>
 */
class LoadSpecialitiesTask extends BaseTask
{
    const SPECIALITIES_ENDPOINT_PATH = '/industry/specialities/';

    /** @var CrestClient */
    private $crestClient;

    /** @var DomainMapper */
    private $mapper;

    /** @var MongoCollectionHelper */
    private $collectionHelper;

    function __construct(CrestClient $crestClient, DocumentManager $dm, DomainMapper $mapper, MongoCollectionHelper $collectionHelper)
    {
        $this->crestClient = $crestClient;
        $this->dm = $dm;
        $this->mapper = $mapper;
        $this->collectionHelper = $collectionHelper;
    }

    public function loadSpecialities()
    {
        $specialities = $this->crestClient->loadCrestResource(self::SPECIALITIES_ENDPOINT_PATH);

        if (!is_array($specialities)) {
            if ($specialities instanceof DomainSpeciality) {
                $specialities = [$specialities];
            } else {
                /** @var object $specialities */
                $this->verifyIsInstanceOf(DomainSpeciality::class, $specialities);
            }
        }

        /** @var array $specialities */
        if (count($specialities) > 0) {
            /** @var DomainSpeciality $domainSpeciality */
            foreach ($specialities as $domainSpeciality) {
                $this->verifyIsInstanceOf(DomainSpeciality::class, $domainSpeciality);
                $documentSpeciality = new IndustrySpeciality();
                $this->mapper->map($domainSpeciality, $documentSpeciality);

                $this->dm->persist($documentSpeciality);
            }

            $this->collectionHelper->purgeMongoCollection(IndustrySpeciality::class);
            $this->dm->flush();
        }
    }
}