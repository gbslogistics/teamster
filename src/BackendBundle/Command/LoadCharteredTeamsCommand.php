<?php

namespace GbsLogistics\Teamster\BackendBundle\Command;
use GbsLogistics\Teamster\BackendBundle\Task\LoadTeamsTask;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Executes loading chartered teams task.
 *
 * @author Querns <querns@gbs.io>
 */
class LoadCharteredTeamsCommand extends Command
{
    /** @var LoadTeamsTask */
    private $loadTeamsTask;

    function __construct(LoadTeamsTask $loadTeamsTask)
    {
        $this->loadTeamsTask = $loadTeamsTask;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('crest:load-chartered-teams')
            ->setDescription('Loads chartered teams from Carbon REST API');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->loadTeamsTask->loadCharteredTeams();
    }
}

