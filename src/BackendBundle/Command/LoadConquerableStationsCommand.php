<?php

namespace GbsLogistics\Teamster\BackendBundle\Command;


use GbsLogistics\Teamster\BackendBundle\Task\LoadConquerableStationsTask;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadConquerableStationsCommand extends Command
{
    /** @var LoadConquerableStationsTask */
    private $loadConquerableStationsTask;

    function __construct(LoadConquerableStationsTask $loadConquerableStationsTask)
    {
        $this->loadConquerableStationsTask = $loadConquerableStationsTask;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('eveapi:load-conquerable-stations')
            ->setDescription('Loads conquerable station list from the Eve API');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->loadConquerableStationsTask->loadConquerableStations();
    }
}