<?php

namespace GbsLogistics\Teamster\BackendBundle\Command;
use GbsLogistics\Teamster\BackendBundle\Task\LoadSpecialitiesTask;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * @author Querns <querns@gbs.io>
 */
class LoadSpecialitiesCommand extends Command
{
    /** @var LoadSpecialitiesTask */
    private $loadSpecialitiesTask;

    function __construct(LoadSpecialitiesTask $loadTeamsTask)
    {
        $this->loadSpecialitiesTask = $loadTeamsTask;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('crest:load-specialities')
            ->setDescription('Loads industry specialities from Carbon REST API');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->loadSpecialitiesTask->loadSpecialities();
    }
} 