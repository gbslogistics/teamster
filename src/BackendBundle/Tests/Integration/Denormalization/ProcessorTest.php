<?php

namespace GbsLogistics\Teamster\BackendBundle\Tests\Integration\Denormalization;

use Doctrine\ODM\MongoDB\DocumentManager;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Processor;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Routine\SolarSystemGeographyRoutine;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustryTeam;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystem;
use GbsLogistics\Teamster\DocumentBundle\Tests\Database\DocumentManagerFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @group integration
 */
class ProcessorTest extends WebTestCase
{

    /** @var Processor */
    private $sut;

    /** @var DocumentManager */
    private $dm;

    public function setUp()
    {
        $client = self::createClient();
        $container = $client->getContainer();

        $this->sut = $container->get('teamster_backend.denormalization.processor');
    }

    /** @dataProvider testDenormalizationProvider */
    public function testDenormalization($solarSystemId, $constellationId, $regionId, $expectedActivityTypes)
    {
        $team = new IndustryTeam();
        $solarSystem = new SolarSystem();
        $solarSystem->setSolarSystemId($solarSystemId);
        $team->setSolarSystem($solarSystem);

        $this->sut->queueObject($team);
        $this->sut->processQueue();

        $geography = $team->getSolarSystemGeography();
        $this->assertEquals($constellationId, $geography->getConstellationId());
        $this->assertEquals($regionId, $geography->getRegionId());

        $retrievedActivityTypes = $geography->getFacilityTypes();
        foreach ($expectedActivityTypes as $activityId) {
            $this->assertContains($activityId, $retrievedActivityTypes);
        }

        $this->assertCount(count($expectedActivityTypes), $retrievedActivityTypes);
    }

    public function testDenormalizationProvider()
    {
        /** @noinspection SpellCheckingInspection */
        return [
            // Soshin, an empire station with research slots
            [ 30000175, 20000025, 10000002, [5, 4, 3, 1, 8] ],

            // II-5O9, a conquerable outpost with research
            [ 30002907, 20000426, 10000035, [7, 1, 4, 3, 5, 8] ],
        ];
    }
}
 