<?php

namespace GbsLogistics\Teamster\BackendBundle\Tests\Integration\Task;


use BCC\AutoMapperBundle\Mapper\Mapper;
use Doctrine\ODM\MongoDB\PersistentCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\SimplifiedYamlDriver;
use Doctrine\ORM\Tools\Setup;
use GbsLogistics\Doramad\SerializerFactory;
use GbsLogistics\SdeEntityBundle\Entity\MapSolarSystem;
use GbsLogistics\SdeEntityBundle\Entity\RamActivity;
use GbsLogistics\SdeEntityBundle\Entity\RamAssemblyLineStation;
use GbsLogistics\SdeEntityBundle\Entity\RamInstallationTypeContents;
use GbsLogistics\SdeEntityBundleUtil\EntityManagerHelper;
use GbsLogistics\Teamster\BackendBundle\CrestClient;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Processor;
use GbsLogistics\Teamster\BackendBundle\Denormalization\ProxyStorage;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Routine\CharacterAffiliationRoutine;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Routine\IndustryActivityRoutine;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Routine\SolarSystemGeographyRoutine;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Routine\WorkerBonusTypeRoutine;
use GbsLogistics\Teamster\BackendBundle\Factory\BatchFactory;
use GbsLogistics\Teamster\BackendBundle\OutpostRamActivityDictionary;
use GbsLogistics\Teamster\BackendBundle\Task\LoadTeamsTask;
use GbsLogistics\Teamster\DocumentBundle\Document\ConquerableStation;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustryTeam;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystem;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemBid;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemGeography;
use GbsLogistics\Teamster\DocumentBundle\Document\Worker;
use GbsLogistics\Teamster\DocumentBundle\DomainMapper;
use GbsLogistics\Teamster\DocumentBundle\Tests\Database\BaseDatabaseTestCase;

/**
 * @group database
 */
class LoadTeamsTaskTest extends BaseDatabaseTestCase
{
    const SINGLE_TEAM_ID = 2771;
    const SINGLE_TEAM_FILE_PATH = '/../../fixtures/single_team_auction.json';

    /** @var LoadTeamsTask */
    private $sut;

    /** @var CrestClient */
    private $crestClient;

    /** @var BatchFactory */
    private $batchFactory;

    /** @var Mapper */
    private $mapper;

    /** @var Processor */
    private $processor;

    public function setUp()
    {
        parent::setUp();

        $em = $this->getEntityManager();

        /** @noinspection SpellCheckingInspection */
        $this->crestClient = new CrestClient(
            [],
            SerializerFactory::getSerializer(),
            'http://public-crest.eveonline.com'
        );

        $this->batchFactory = new BatchFactory();
        $this->mapper = new DomainMapper();
        $this->processor = new Processor();
        $this->processor->setProxyStorage(new ProxyStorage());
        $this->processor->registerRoutine(new SolarSystemGeographyRoutine(
            $em->getRepository(MapSolarSystem::class),
            $em->getRepository(RamAssemblyLineStation::class),
            $this->dm->getRepository(ConquerableStation::class),
            new OutpostRamActivityDictionary($em->getRepository(RamInstallationTypeContents::class))
        ));
        $this->processor->registerRoutine(new CharacterAffiliationRoutine());
        $this->processor->registerRoutine(new IndustryActivityRoutine($em->getRepository(RamActivity::class)));
        $this->processor->registerRoutine(new WorkerBonusTypeRoutine());

        $this->sut = new LoadTeamsTask(
            $this->crestClient,
            $this->dm,
            $this->batchFactory,
            $this->mapper,
            $this->processor
        );
    }

    public function testLoadCharteredTeams()
    {
        $totalNumberOfExpectedRecords = $this->attachMockResponseToClient(
			$this->crestClient,
            __DIR__ . '/../../fixtures/industry_teams_response.txt',
            __DIR__ . '/../../fixtures/industry_teams_header.txt'
        );

        $this->sut->loadCharteredTeams();
        $this->assertRecordsImportedProperly($totalNumberOfExpectedRecords);
    }

    public function testLoadAuctionTeams()
    {
        $totalNumberOfExpectedRecords = $this->attachMockResponseToClient(
			$this->crestClient,
            __DIR__ . '/../../fixtures/industry_teams_auction_response.txt',
            __DIR__ . '/../../fixtures/industry_teams_auction_header.txt'
        );

        $this->sut->loadAuctionTeams();
        $this->assertRecordsImportedProperly($totalNumberOfExpectedRecords);
    }

    public function testLoadingOldEntryCausesUpdate()
    {
        $documentTeam = $this->loadSingleTeam();

        /** @var SolarSystemBid $solarSystemBid */
        $solarSystemBid = $documentTeam->getSolarSystemBids()[0];
        $characterBids = $solarSystemBid->getCharacterBids();
        $solarSystemBid->setCharacterBids([]);
        $this->dm->persist($documentTeam);
        $this->dm->flush();
        $this->dm->clear();

        $this->attachMockResponseToClient(
			$this->crestClient,
            __DIR__ . self::SINGLE_TEAM_FILE_PATH,
            __DIR__ . '/../../fixtures/single_team_auction_headers.txt'
        );

        $this->sut->loadAuctionTeams();

        /** @var IndustryTeam $retrievedTeam */
        $retrievedTeam = $this->dm->getRepository(IndustryTeam::class)
            ->findOneBy(['teamId' => self::SINGLE_TEAM_ID]);

        $this->assertNotNull($retrievedTeam);
        $this->assertInstanceOf(IndustryTeam::class, $retrievedTeam);
        /** @var SolarSystemBid $retrievedSolarSystemBid */
        $retrievedSolarSystemBid = $retrievedTeam->getSolarSystemBids()[0];
        $this->assertNotCount(0, $retrievedSolarSystemBid->getCharacterBids());
        /** @noinspection PhpUndefinedMethodInspection */
        $this->assertEquals($characterBids, $retrievedSolarSystemBid->getCharacterBids()->toArray());

        $this->assertRecordsImportedProperly(1);
    }

    public function testBidDataIsNotOverwrittenDuringUpdate()
    {
        $documentTeam = $this->loadSingleTeam();

        $this->dm->persist($documentTeam);
        $this->dm->flush($documentTeam);
        $this->dm->detach($documentTeam);

        $this->attachMockResponseToClient(
			$this->crestClient,
            __DIR__ . '/../../fixtures/single_team.json',
            __DIR__ . '/../../fixtures/single_team_headers.txt'
        );

        $this->sut->loadAuctionTeams();

        /** @var IndustryTeam $retrievedTeam */
        $retrievedTeam = $this->dm->getRepository(IndustryTeam::class)
            ->findOneBy(['teamId' => self::SINGLE_TEAM_ID]);

        $this->assertNotNull($retrievedTeam);
        $this->assertInstanceOf(IndustryTeam::class, $retrievedTeam);
        $this->assertNotNull($retrievedTeam->getAuctionExpiryTime());

        /** @var PersistentCollection $solarSystemBids */
        $solarSystemBids = $retrievedTeam->getSolarSystemBids();
        $this->assertNotNull($solarSystemBids);
        $this->assertInstanceOf(PersistentCollection::class, $solarSystemBids);
        $bidArray = $solarSystemBids->toArray();
        $this->assertNotCount(0, $bidArray);
    }

    /**
     * @param $totalNumberOfExpectedRecords
     */
    private function assertRecordsImportedProperly($totalNumberOfExpectedRecords)
    {
        $this->dm->clear();
        $records = $this->dm->getRepository(IndustryTeam::class)
            ->findAll();

        $this->assertInternalType('array', $records);
        $this->assertCount($totalNumberOfExpectedRecords, $records);
        $this->assertContainsOnlyInstancesOf(IndustryTeam::class, $records);

        /** @var IndustryTeam $record */
        foreach ($records as $record) {
            $this->assertContainsOnlyInstancesOf(Worker::class, $record->getWorkers());
            $this->assertInstanceOf(SolarSystem::class, $record->getSolarSystem());

            if (count($record->getSolarSystemBids())) {
                $this->assertContainsOnlyInstancesOf(
                    SolarSystemBid::class,
                    $record->getSolarSystemBids()
                );
            }
        }
    }

    /**
     * @return IndustryTeam
     */
    private function loadSingleTeam()
    {
        $this->markTestSkipped();
        $serializer = SerializerFactory::getSerializer();
        $mapper = new DomainMapper();

        $teamJson = file_get_contents(__DIR__ . self::SINGLE_TEAM_FILE_PATH);
        $team = $serializer->deserialize($teamJson, \GbsLogistics\Doramad\Domain\IndustryTeam::class, 'json');
        $documentTeam = new IndustryTeam();
        $mapper->map($team, $documentTeam);

        $documentTeam->setSolarSystemGeography(new SolarSystemGeography());

        return $documentTeam;
    }

    private function getEntityManager()
    {
        $namespaces = [
            __DIR__ . '/../../../../../vendor/gbslogistics/sde-entity-bundle/src/GbsLogistics/SdeEntityBundle/Resources/config/doctrine' =>
                'GbsLogistics\\SdeEntityBundle\\Entity'
        ];

        $dbParams = [
            'driver' => 'pdo_sqlite',
            'path' => __DIR__ . '/../../../../../../oceanus10.sqlite'
        ];

        $config = Setup::createConfiguration(true);
        $driver = new SimplifiedYamlDriver($namespaces);
        $config->setMetadataDriverImpl($driver);

        return EntityManager::create($dbParams, $config);
    }
}
