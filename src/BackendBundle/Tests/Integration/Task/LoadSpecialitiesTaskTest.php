<?php

namespace GbsLogistics\Teamster\BackendBundle\Tests\Integration\Task;


use GbsLogistics\Doramad\SerializerFactory;
use GbsLogistics\Teamster\BackendBundle\CrestClient;
use GbsLogistics\Teamster\BackendBundle\Task\LoadSpecialitiesTask;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustrySpeciality;
use GbsLogistics\Teamster\DocumentBundle\DomainMapper;
use GbsLogistics\Teamster\DocumentBundle\MongoCollectionHelper;
use GbsLogistics\Teamster\DocumentBundle\Tests\Database\BaseDatabaseTestCase;

/** @group database */
class LoadSpecialitiesTaskTest extends BaseDatabaseTestCase
{
	/** @var LoadSpecialitiesTask */
	private $sut;

	/** @var CrestClient */
	private $crestClient;

	public function setUp()
	{
		parent::setUp();

		/** @noinspection SpellCheckingInspection */
        $this->crestClient = new CrestClient(
            [],
            SerializerFactory::getSerializer(),
            'http://public-crest.eveonline.com'
        );

		$mapper = new DomainMapper();

		$this->sut = new LoadSpecialitiesTask(
			$this->crestClient,
			$this->dm,
			$mapper,
			new MongoCollectionHelper($this->dm)
		);
	}

	public function testLoadSpecialities()
	{
		$totalNumberOfExpectedRecords = $this->attachMockResponseToClient(
			$this->crestClient,
            __DIR__ . '/../../fixtures/industry_speciality_response.txt',
			__DIR__ . '/../../fixtures/industry_speciality_header.txt'
        );

		$this->sut->loadSpecialities();

		$this->dm->clear();
        $records = $this->dm->getRepository(IndustrySpeciality::class)
            ->findAll();

		$this->assertInternalType('array', $records);
		$this->assertCount($totalNumberOfExpectedRecords, $records);
		$this->assertContainsOnlyInstancesOf(IndustrySpeciality::class, $records);

		/** @var IndustrySpeciality $record */
		foreach ($records as $record) {
			$this->assertInstanceOf(IndustrySpeciality::class, $record);
			$this->assertNotNull($record->getName());
			$this->assertNotNull($record->getSpecializationId());
			$this->assertInternalType('array', $record->getGroups());
		}
	}
}
 