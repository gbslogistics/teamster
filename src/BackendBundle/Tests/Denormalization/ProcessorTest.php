<?php


namespace GbsLogistics\Teamster\BackendBundle\Tests\Denormalization;


use GbsLogistics\Teamster\BackendBundle\Denormalization\DenormalizableEntityInterface;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Processor;
use GbsLogistics\Teamster\DocumentBundle\Model\Proxy;
use GbsLogistics\Teamster\BackendBundle\Denormalization\ProxyStorage;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Routine\DenormalizationRoutineInterface;
use GbsLogistics\Teamster\BackendBundle\Tests\ProxyTestClass;
use Phake;


class ProcessorTest extends \PHPUnit_Framework_TestCase
{
    const ROUTINE_CLASS = 'registered_class';
    const DISCRIMINATOR = 1;
    const DENORMALIZED_VALUE = 'gbs';

    /** @var Processor */
    private $sut;

    /**
     * @var ProxyStorage
     * @Mock
     */
    private $proxyStorage;

    /**
     * @var \GbsLogistics\Teamster\DocumentBundle\Model\Proxy
     * @Mock
     */
    private $proxy;

    /**
     * @var ProxyTestClass
     * @Mock
     */
    private $testEntity;

    /**
     * @var DenormalizableEntityInterface
     * @Mock
     */
    private $denormalizableEntity;

    /**
     * @var DenormalizationRoutineInterface
     * @Mock
     */
    private $denormalizationRoutine;

    public function setUp()
    {
        Phake::initAnnotations($this);
        $this->sut = new Processor();
        $this->sut->setProxyStorage($this->proxyStorage);
    }

    public function testAddProxy()
    {
        $this->mockDenormalizableEntity([$this->proxy, $this->proxy, new \stdClass]);
        $this->sut->queueObject($this->denormalizableEntity);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::verify($this->proxyStorage, Phake::times(2))->addProxy(Phake::anyParameters());

        /** @noinspection PhpParamsInspection */
        Phake::verifyNoFurtherInteraction($this->proxyStorage);
    }

    /**
     * @expectedException \RuntimeException
     */
    public function testProcessQueueThrowsWhenRoutineIsNotRegistered()
    {
        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->proxyStorage)->getRoutineClasses()
            ->thenReturn(['not_registered_class']);

        $this->sut->processQueue();
    }

    public function testProcessQueue()
    {
        $routineClass = get_class($this->denormalizationRoutine);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->proxyStorage)->getRoutineClasses()
            ->thenReturn([$routineClass]);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->proxyStorage)->getDiscriminators($routineClass)
            ->thenReturn([self::DISCRIMINATOR]);

        /** @noinspection PhpUnusedParameterInspection */
        $proxy = new Proxy(
            $routineClass,
            function () {
                return self::DISCRIMINATOR;
            },
            function (\stdClass $result) {
                $this->testEntity->setDenormalizedValue(self::DENORMALIZED_VALUE);
            }
        );

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->proxyStorage)->getProxies($routineClass, self::DISCRIMINATOR)
            ->thenReturn([$proxy]);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->denormalizationRoutine)
            ->extractDiscriminatorFromResult($this->isInstanceOf(\stdClass::class))
            ->thenReturn(self::DISCRIMINATOR);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->denormalizationRoutine)
            ->performDenormalizationLookup(Phake::anyParameters())
            ->thenReturn([new \stdClass()]);

        $this->sut->registerRoutine($this->denormalizationRoutine);
        $this->sut->processQueue();

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::verify($this->testEntity)->setDenormalizedValue(self::DENORMALIZED_VALUE);
    }

    private function mockDenormalizableEntity(array $proxies)
    {
        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->denormalizableEntity)->getDenormalizationProxies()
            ->thenReturn($proxies);
    }
}
 