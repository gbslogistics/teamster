<?php


namespace GbsLogistics\Teamster\BackendBundle\Tests\Denormalization\Routine;


use GbsLogistics\Doramad\Domain\SpecializationTypeEnum;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Routine\WorkerBonusTypeRoutine;
use GbsLogistics\Teamster\BackendBundle\Model\WorkerBonusTypeRoutineResult;

class WorkerBonusTypeRoutineTest extends \PHPUnit_Framework_TestCase
{
    /** @var WorkerBonusTypeRoutine */
    private $sut;

    public function setUp()
    {
        $this->sut = new WorkerBonusTypeRoutine();
    }

    public function testPerformDenormalizationLookup()
    {
        $results = $this->sut->performDenormalizationLookup([SpecializationTypeEnum::ME, SpecializationTypeEnum::TE]);
        $i = 1;
        /** @var WorkerBonusTypeRoutineResult $result */
        foreach ($results as $result) {
            $this->assertInstanceOf(WorkerBonusTypeRoutineResult::class, $result);

            $discriminator = $result->getDiscriminator();

            if (SpecializationTypeEnum::ME === $discriminator) {
                $this->assertEquals('ME', $result->getBonusIdString());
            } elseif (SpecializationTypeEnum::TE === $discriminator) {
                $this->assertEquals('TE', $result->getBonusIdString());
            } else {
                $this->fail(sprintf(
                    'Expected result #%s from denormalization lookup to return a valid discriminator, '
                    . 'got %s (type: %s) instead.',
                    $i,
                    $discriminator,
                    gettype($discriminator)
                ));
            }

            $i++;
        }
    }
}
 