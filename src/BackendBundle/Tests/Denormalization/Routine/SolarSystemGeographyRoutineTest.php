<?php

namespace GbsLogistics\Teamster\BackendBundle\Tests\Denormalization\Routine;

use Doctrine\ORM\EntityRepository;
use GbsLogistics\SdeEntityBundle\Entity\MapConstellation;
use GbsLogistics\SdeEntityBundle\Entity\MapLocationWormholeClass;
use GbsLogistics\SdeEntityBundle\Entity\MapRegion;
use GbsLogistics\SdeEntityBundle\Entity\MapSolarSystem;
use GbsLogistics\SdeEntityBundle\Entity\RamAssemblyLineStation;
use GbsLogistics\SdeEntityBundle\Entity\RamAssemblyLineType;
use GbsLogistics\SdeEntityBundle\Entity\Repository\RamAssemblyLineStationRepository;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Routine\SolarSystemGeographyRoutine;
use GbsLogistics\Teamster\BackendBundle\Model\SolarSystemGeographyResult;
use GbsLogistics\Teamster\BackendBundle\OutpostRamActivityDictionary;
use GbsLogistics\Teamster\DocumentBundle\Document\ConquerableStation;
use GbsLogistics\Teamster\DocumentBundle\Document\Repository\ConquerableStationRepository;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemGeography;
use Phake;

class SolarSystemGeographyRoutineTest extends \PHPUnit_Framework_TestCase
{
    const NPC_SOLAR_SYSTEM_ID = 12345;
    const CONQUERABLE_SOLAR_SYSTEM_ID = 54321;
    const STATION_TYPE_ID = 6789;
    const CONQUERABLE_ACTIVITY_ID = 1;
    const NPC_ACTIVITY_ID = 2;
    const NPC_CONSTELLATION_ID = 438765;
    const NPC_REGION_ID = 7654642;
    const CONQUERABLE_CONSTELLATION_ID = 765433;
    const CONQUERABLE_REGION_ID = 2394732;
    const WORMHOLE_CLASS_ID_ONE = 1;
    const WORMHOLE_CLASS_ID_TWO = 7;

    /** @var SolarSystemGeographyRoutine */
    private $sut;

    /**
     * @var EntityRepository
     * @Mock
     */
    private $mapSolarSystemRepository;

    /**
     * @var RamAssemblyLineStationRepository
     * @Mock
     */
    private $ramAssemblyLineStationRepository;

    /**
     * @var ConquerableStationRepository
     * @Mock
     */
    private $conquerableStationRepository;

    /**
     * @var OutpostRamActivityDictionary
     * @Mock
     */
    private $outpostRamActivityDictionary;

    public function setUp()
    {
        Phake::initAnnotations($this);
        $this->sut = new SolarSystemGeographyRoutine(
            $this->mapSolarSystemRepository,
            $this->ramAssemblyLineStationRepository,
            $this->conquerableStationRepository,
            $this->outpostRamActivityDictionary
        );
    }

    public function testPerformDenormalization()
    {
        $discriminators = [self::NPC_SOLAR_SYSTEM_ID, self::CONQUERABLE_SOLAR_SYSTEM_ID];
        $ramAssemblyLineStation = new RamAssemblyLineStation();
        $conquerableStation = new ConquerableStation();
        $conquerableStation->setStationTypeID(self::STATION_TYPE_ID);
        $assemblyLineType = new RamAssemblyLineType();
        $assemblyLineType->setActivityID(self::NPC_ACTIVITY_ID);
        $ramAssemblyLineStation->setAssemblyLineType($assemblyLineType);
        $ramAssemblyLineStation->setSolarSystemID(self::NPC_SOLAR_SYSTEM_ID);
        $constellation = new MapConstellation();
        $regionOne = new MapRegion();
        $regionTwo = new MapRegion();
        $wormholeClassOne = new MapLocationWormholeClass();
        $wormholeClassOne->setWormholeClassID(self::WORMHOLE_CLASS_ID_ONE);
        $wormholeClassTwo = new MapLocationWormholeClass();
        $wormholeClassTwo->setWormholeClassID(self::WORMHOLE_CLASS_ID_TWO);
        $regionOne->setWormholeClass($wormholeClassOne);
        $regionTwo->setWormholeClass($wormholeClassTwo);
        $npcSolarSystem = new MapSolarSystem();
        $conquerableSolarSystem = new MapSolarSystem();
        $npcSolarSystem->setSolarSystemID(self::NPC_SOLAR_SYSTEM_ID);
        $npcSolarSystem->setConstellationID(self::NPC_CONSTELLATION_ID);
        $npcSolarSystem->setRegionID(self::NPC_REGION_ID);
        $conquerableSolarSystem->setSolarSystemID(self::CONQUERABLE_SOLAR_SYSTEM_ID);
        $conquerableSolarSystem->setConstellationID(self::CONQUERABLE_CONSTELLATION_ID);
        $conquerableSolarSystem->setRegionID(self::CONQUERABLE_REGION_ID);
        $npcSolarSystem->setConstellation($constellation);
        $conquerableSolarSystem->setConstellation($constellation);
        $npcSolarSystem->setRegion($regionOne);
        $conquerableSolarSystem->setRegion($regionTwo);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->mapSolarSystemRepository)->findBy([ 'solarSystemID' => $discriminators ])
            ->thenReturn([ $npcSolarSystem, $conquerableSolarSystem ]);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->conquerableStationRepository)->fetchBySolarSystemIdsIndexed($discriminators)
            ->thenReturn([self::CONQUERABLE_SOLAR_SYSTEM_ID => $conquerableStation]);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->ramAssemblyLineStationRepository)->findBy($this->arrayHasKey('solarSystemID'))
            ->thenReturn([ $ramAssemblyLineStation ]);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->outpostRamActivityDictionary)->getRamActivities(self::STATION_TYPE_ID)
            ->thenReturn([self::CONQUERABLE_ACTIVITY_ID]);

        /** @var SolarSystemGeography $result */
        foreach ($this->sut->performDenormalizationLookup($discriminators) as $result) {
            $this->assertInstanceOf(SolarSystemGeography::class, $result);

            if ($result->getConstellationId() === self::NPC_CONSTELLATION_ID) {
                $this->assertEquals(self::WORMHOLE_CLASS_ID_ONE, $result->getWormholeClass());
                $this->assertEquals([ self::NPC_ACTIVITY_ID ], $result->getFacilityTypes());
            } elseif ($result->getConstellationId() === self::CONQUERABLE_CONSTELLATION_ID) {
                $this->assertEquals([ self::CONQUERABLE_ACTIVITY_ID ], $result->getFacilityTypes());
                $this->assertNull($result->getWormholeClass());
            } else {
                $this->fail('Unexpected solar system id returned during denormalization lookup.');
            }
        }

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::verify($this->conquerableStationRepository)->fetchBySolarSystemIdsIndexed($discriminators);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::verify($this->ramAssemblyLineStationRepository)->findBy($this->arrayHasKey('solarSystemID'));
    }
}
 