<?php

namespace GbsLogistics\Teamster\BackendBundle\Tests\Denormalization;

use GbsLogistics\Teamster\DocumentBundle\Model\Proxy;
use GbsLogistics\Teamster\BackendBundle\Denormalization\ProxyStorage;
use GbsLogistics\Teamster\BackendBundle\Tests\ProxyTestClass;
use Phake;

class ProxyStorageTest extends \PHPUnit_Framework_TestCase
{
    const ROUTINE_CLASS = 'gbs_one';
    const DENORMALIZED_VALUE = 'denormalized value';
    const DISCRIMINATOR_ONE = 1;
    const DISCRIMINATOR_TWO = 2;

    /** @var ProxyStorage */
    private $sut;

    /**
     * @var ProxyTestClass
     * @Mock
     */
    private $testCaseOne;

    /**
     * @var ProxyTestClass
     * @Mock
     */
    private $testCaseTwo;

    public function setUp()
    {
        Phake::initAnnotations($this);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->testCaseOne)->getDiscriminator()->thenReturn(self::DISCRIMINATOR_ONE);
        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->testCaseTwo)->getDiscriminator()->thenReturn(self::DISCRIMINATOR_TWO);

        /** @noinspection PhpUnusedParameterInspection */
        $proxyOne = new \GbsLogistics\Teamster\DocumentBundle\Model\Proxy(
            self::ROUTINE_CLASS,
            function () {
                return $this->testCaseOne->getDiscriminator();
            },
            function (\stdClass $result) {
                $this->testCaseOne->setDenormalizedValue(self::DENORMALIZED_VALUE);
            }
        );

        /** @noinspection PhpUnusedParameterInspection */
        $proxyTwo = new Proxy(
            self::ROUTINE_CLASS,
            function () {
                return $this->testCaseTwo->getDiscriminator();
            },
            function (\stdClass $result) {
                $this->testCaseTwo->setDenormalizedValue(self::DENORMALIZED_VALUE);
            }
        );

        $this->sut = new ProxyStorage();
        $this->sut->addProxy($proxyOne);
        $this->sut->addProxy($proxyTwo);
    }

    public function testGetRoutineClasses()
    {
        $routineClasses = $this->sut->getRoutineClasses();

        $this->assertEquals([self::ROUTINE_CLASS], $routineClasses);
    }

    public function testGetDiscriminators()
    {
        /** @noinspection PhpParamsInspection */
        Phake::verifyNoInteraction($this->testCaseOne);
        /** @noinspection PhpParamsInspection */
        Phake::verifyNoInteraction($this->testCaseTwo);

        $discriminators = [];
        foreach ($this->sut->getDiscriminators(self::ROUTINE_CLASS) as $discriminator) {
            $discriminators[] = $discriminator;
        }

        $this->assertEquals([self::DISCRIMINATOR_ONE, self::DISCRIMINATOR_TWO], $discriminators);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::verify($this->testCaseOne)->getDiscriminator();

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::verify($this->testCaseOne)->getDiscriminator();
    }

    public function testGetProxies()
    {
        /** @var Proxy $proxy */
        foreach ($this->sut->getProxies(self::ROUTINE_CLASS, self::DISCRIMINATOR_ONE) as $proxy) {
            $denormalizationCallback = $proxy->getDenormalizationCallback();
            $denormalizationCallback(new \stdClass());
        }

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::verify($this->testCaseOne)->getDiscriminator();
        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::verify($this->testCaseOne)->setDenormalizedValue(self::DENORMALIZED_VALUE);
        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::verify($this->testCaseTwo)->getDiscriminator();
        /** @noinspection PhpParamsInspection */
        Phake::verifyNoFurtherInteraction($this->testCaseTwo);
    }

    /**
     * @expectedException \RuntimeException
     */
    public function testReturningNullFromDiscriminatorCallbackThrows()
    {
        $proxyThree = new Proxy(
            self::ROUTINE_CLASS,
            function () {
                return null;
            },
            function () { }
        );

        $this->sut->addProxy($proxyThree);

        $this->sut->getDiscriminators(self::ROUTINE_CLASS);
    }
}
 