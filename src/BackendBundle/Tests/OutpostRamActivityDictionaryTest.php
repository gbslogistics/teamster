<?php

namespace GbsLogistics\Teamster\BackendBundle\Tests;

use Doctrine\ORM\EntityRepository;
use GbsLogistics\SdeEntityBundle\Entity\RamAssemblyLineType;
use GbsLogistics\SdeEntityBundle\Entity\RamInstallationTypeContents;
use GbsLogistics\Teamster\BackendBundle\OutpostRamActivityDictionary;
use Phake;

class OutpostRamActivityDictionaryTest extends \PHPUnit_Framework_TestCase
{
    const ASSEMBLY_LINE_TYPE_ID = 12345;
    const ACTIVITY_ONE = 1;
    const ACTIVITY_TWO = 2;

    /** @var OutpostRamActivityDictionary */
    private $sut;

    /**
     * @var EntityRepository
     * @Mock
     */
    private $repository;

    public function setUp()
    {
        Phake::initAnnotations($this);

        $this->sut = new OutpostRamActivityDictionary($this->repository);
    }

    public function testGetRamActivitiesEnforcingUniqueness()
    {
        $contentsOne = Phake::mock(RamInstallationTypeContents::class);
        $contentsTwo = Phake::mock(RamInstallationTypeContents::class);
        $contentsThree = Phake::mock(RamInstallationTypeContents::class);

        $assemblyLineOne = Phake::mock(RamAssemblyLineType::class);
        $assemblyLineTwo = Phake::mock(RamAssemblyLineType::class);
        $assemblyLineThree = Phake::mock(RamAssemblyLineType::class);

        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($contentsOne)->getInstallationTypeID()->thenReturn(self::ASSEMBLY_LINE_TYPE_ID);
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($contentsTwo)->getInstallationTypeID()->thenReturn(self::ASSEMBLY_LINE_TYPE_ID);
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($contentsThree)->getInstallationTypeID()->thenReturn(self::ASSEMBLY_LINE_TYPE_ID);

        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($contentsOne)->getAssemblyLineType()->thenReturn($assemblyLineOne);
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($contentsTwo)->getAssemblyLineType()->thenReturn($assemblyLineTwo);
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($contentsThree)->getAssemblyLineType()->thenReturn($assemblyLineThree);

        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($assemblyLineOne)->getActivityID()->thenReturn(self::ACTIVITY_ONE);
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($assemblyLineTwo)->getActivityID()->thenReturn(self::ACTIVITY_ONE);
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($assemblyLineThree)->getActivityID()->thenReturn(self::ACTIVITY_TWO);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->repository)->findAll()->thenReturn([$contentsOne, $contentsTwo, $contentsThree]);

        $activities = $this->sut->getRamActivities(self::ASSEMBLY_LINE_TYPE_ID);

        $this->assertEquals([self::ACTIVITY_ONE, self::ACTIVITY_TWO], $activities);
    }
}
 