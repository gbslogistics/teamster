<?php

namespace GbsLogistics\Teamster\BackendBundle\Tests;


class ProxyTestClass
{
    private $discriminator;

    private $denormalizedValue;

    /**
     * @return mixed
     */
    public function getDiscriminator()
    {
        return $this->discriminator;
    }

    /**
     * @param mixed $discriminator
     */
    public function setDiscriminator($discriminator)
    {
        $this->discriminator = $discriminator;
    }

    /**
     * @return mixed
     */
    public function getDenormalizedValue()
    {
        return $this->denormalizedValue;
    }

    /**
     * @param mixed $denormalizedValue
     */
    public function setDenormalizedValue($denormalizedValue)
    {
        $this->denormalizedValue = $denormalizedValue;
    }

}
