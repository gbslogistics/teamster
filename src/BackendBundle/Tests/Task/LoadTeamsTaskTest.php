<?php

namespace GbsLogistics\Teamster\BackendBundle\Tests\Task;


use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\DocumentRepository;
use GbsLogistics\Doramad\Domain\IndustryTeam as DomainTeam;
use GbsLogistics\Teamster\BackendBundle\CrestClient;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Processor;
use GbsLogistics\Teamster\BackendBundle\Factory\BatchFactory;
use GbsLogistics\Teamster\BackendBundle\Task\LoadTeamsTask;
use GbsLogistics\Teamster\DocumentBundle\Document\Batch;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustryTeam;
use GbsLogistics\Teamster\DocumentBundle\DomainMapper;
use Phake;

class LoadTeamsTaskTest extends \PHPUnit_Framework_TestCase
{
    const TEAM_ID = 12345;
    const BATCH_NUMBER = 1234567890;

    /** @var LoadTeamsTask */
    private $sut;

    /**
     * @var CrestClient
     * @Mock
     */
    private $crestClient;

    /**
     * @var BatchFactory
     * @Mock
     */
    private $batchFactory;

    /**
     * @var DomainMapper
     * @Mock
     */
    private $mapper;

    /**
     * @var DocumentManager
     * @Mock
     */
    private $dm;

    /**
     * @var DocumentRepository
     * @Mock
     */
    private $repository;

    /**
     * @var Batch
     * @Mock
     */
    private $batch;

    /**
     * @var Processor
     * @Mock
     */
    private $processor;

    public function setUp()
    {
        Phake::initAnnotations($this);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->dm)->getRepository(IndustryTeam::class)
            ->thenReturn($this->repository);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->batchFactory)->createCharteredTeamBatch()
            ->thenReturn($this->batch);

		/** @noinspection PhpParamsInspection */
		/** @noinspection PhpUndefinedMethodInspection */
		Phake::when($this->batchFactory)->createAuctionTeamBatch()
			->thenReturn($this->batch);

        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->batch)->getBatchNumber()->thenReturn(self::BATCH_NUMBER);

        $this->sut = new LoadTeamsTask(
            $this->crestClient,
            $this->dm,
            $this->batchFactory,
            $this->mapper,
            $this->processor
        );
    }

    public function testLoadTeamsPersistsNewTeams()
    {
        $domainTeam = new DomainTeam();
        $this->mockRepositoryFindOneBy(self::TEAM_ID, null);
        $this->mockCrestClientLoadCrestResource($domainTeam);

        $this->sut->loadCharteredTeams();

        Phake::verify($this->dm)->persist($this->batch);
        /** @var IndustryTeam $documentTeam */
        // TODO fix this test to handle post denormalization callbacks
/*        Phake::verify($this->dm)->persist(
            Phake::capture($documentTeam)
                ->when($this->isInstanceOf(IndustryTeam::class))
        );
        Phake::verify($this->mapper)->map($domainTeam, $documentTeam);

        $this->assertInstanceOf(IndustryTeam::class, $documentTeam);
        $this->assertEquals(self::BATCH_NUMBER, $documentTeam->getBatchNumber());*/
    }

    public function testLoadTeamsUpdatesOldTeams()
    {
        $domainTeam = new DomainTeam();
        $documentTeam = new IndustryTeam();
        $this->mockRepositoryFindOneBy(self::TEAM_ID, $documentTeam);
        $this->mockCrestClientLoadCrestResource($domainTeam);

        $this->sut->loadCharteredTeams();

        Phake::verify($this->mapper)->map(
            $this->isInstanceOf(DomainTeam::class),
            $this->isInstanceOf(IndustryTeam::class)
        );

		$this->assertEquals(self::BATCH_NUMBER, $documentTeam->getBatchNumber());
    }

    /** @expectedException \RuntimeException */
    public function testImproperReturnObjectFromCrest()
    {
        $this->mockCrestClientLoadCrestResource(new \stdClass());
        $this->sut->loadCharteredTeams();
    }

    /** @expectedException \RuntimeException */
    public function testImproperReturnObjectInArrayFromCrest()
    {
        $this->mockCrestClientLoadCrestResource([new \stdClass()]);
        $this->sut->loadCharteredTeams();
    }

    /** @expectedException \RuntimeException */
    public function testImproperReturnTypeFromCrest()
    {
        $this->mockCrestClientLoadCrestResource('wrong item');
        $this->sut->loadCharteredTeams();
    }

    /** @expectedException \RuntimeException */
    public function testImproperTypeInArrayFromCrest()
    {
        $this->mockCrestClientLoadCrestResource(['wrong item']);
        $this->sut->loadCharteredTeams();
    }

    private function mockRepositoryFindOneBy($teamId, $returnedValue)
    {
        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->repository)->findOneBy($this->isType('array'))
            ->thenReturn($returnedValue);
    }

    private function mockCrestClientLoadCrestResource($returnedValue)
    {
        /** @noinspection PhpParamsInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Phake::when($this->crestClient)
            ->loadCrestResource(LoadTeamsTask::CHARTERED_TEAMS_ENDPOINT_PATH)
            ->thenReturn(is_array($returnedValue) ? $returnedValue : [$returnedValue]);
    }
}
