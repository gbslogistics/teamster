<?php

namespace GbsLogistics\Teamster\BackendBundle\Tests\Task;


use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Query\Builder;
use GbsLogistics\Doramad\Domain\IndustrySpeciality;
use GbsLogistics\Teamster\BackendBundle\CrestClient;
use GbsLogistics\Teamster\BackendBundle\Task\LoadSpecialitiesTask;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustrySpeciality as DocumentSpeciality;
use GbsLogistics\Teamster\DocumentBundle\DomainMapper;
use GbsLogistics\Teamster\DocumentBundle\MongoCollectionHelper;
use Phake;

class LoadSpecialitiesTaskTest extends \PHPUnit_Framework_TestCase
{
	/** @var LoadSpecialitiesTask */
    private $sut;

    /**
     * @var CrestClient
     * @Mock
     */
    private $crestClient;

	/**
     * @var DomainMapper
     * @Mock
     */
    private $mapper;

    /**
     * @var DocumentManager
     * @Mock
     */
    private $dm;

	/**
	 * @var MongoCollectionHelper
	 * @Mock
	 */
	private $collectionHelper;

	public function setUp()
	{
		Phake::initAnnotations($this);

        $this->sut = new LoadSpecialitiesTask(
            $this->crestClient,
            $this->dm,
            $this->mapper,
			$this->collectionHelper
        );
	}

	/**
	 * @dataProvider loadSpecialitiesThrowsWhenNonSpecialityReturnedProvider
	 * @expectedException \RuntimeException
	 * @param $returnedValue
	 */
	public function testLoadSpecialitiesThrowsWhenNonSpecialityReturned($returnedValue)
	{
		/** @noinspection PhpUndefinedMethodInspection */
		/** @noinspection PhpParamsInspection */
		Phake::when($this->crestClient)->loadCrestResource(Phake::anyParameters())
			->thenReturn($returnedValue);

		$this->sut->loadSpecialities();
	}

	public function loadSpecialitiesThrowsWhenNonSpecialityReturnedProvider()
	{
		return [
			[ new \stdClass ],
			[ [ new \stdClass ] ],
		];
	}

	public function testLoadSpecialities()
	{
		$domainSpeciality = new IndustrySpeciality();

		/** @noinspection PhpUndefinedMethodInspection */
		/** @noinspection PhpParamsInspection */
		Phake::when($this->crestClient)->loadCrestResource(Phake::anyParameters())
			->thenReturn([$domainSpeciality]);

		$this->sut->loadSpecialities();

		/** @noinspection PhpParamsInspection */
		/** @noinspection PhpUndefinedMethodInspection */
		Phake::verify($this->dm)->persist(Phake::capture($documentSpeciality));
		/** @noinspection PhpParamsInspection */
		/** @noinspection PhpUndefinedMethodInspection */
		Phake::verify($this->collectionHelper)->purgeMongoCollection(DocumentSpeciality::class);

		/** @var DocumentSpeciality $documentSpeciality */
		$this->assertInstanceOf(DocumentSpeciality::class, $documentSpeciality);
	}
}
 