<?php

namespace GbsLogistics\Teamster\BackendBundle\Model;

class WorkerBonusTypeRoutineResult
{
    private $discriminator;

    private $bonusIdString;

    function __construct($bonusIdString, $discriminator)
    {
        $this->bonusIdString = $bonusIdString;
        $this->discriminator = $discriminator;
    }

    /**
     * @return mixed
     */
    public function getBonusIdString()
    {
        return $this->bonusIdString;
    }

    /**
     * @return mixed
     */
    public function getDiscriminator()
    {
        return $this->discriminator;
    }
}

