<?php

namespace GbsLogistics\Teamster\BackendBundle\DependencyInjection;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ConfigurationExtensionInterface;


/**
 *
 *
 * @author Querns <querns@gbs.io>
 */
class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder();
        $builder->root('teamster_backend')
            ->children()
                ->scalarNode('crest_api_host')->isRequired()->cannotBeEmpty()->end()
            ->end();

        return $builder;
    }
}
