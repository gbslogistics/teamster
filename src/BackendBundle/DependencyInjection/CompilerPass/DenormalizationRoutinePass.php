<?php

namespace GbsLogistics\Teamster\BackendBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class DenormalizationRoutinePass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @api
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('teamster_backend.denormalization.processor')) {
            return;
        }

        $definition = $container->getDefinition(
            'teamster_backend.denormalization.processor'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'gbslogistics.teamster.denormalization_routine'
        );
        foreach ($taggedServices as $id => $attributes) {
            $definition->addMethodCall(
                'registerRoutine',
                array(new Reference($id))
            );
        }
    }
}