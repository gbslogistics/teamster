<?php

namespace GbsLogistics\Teamster\BackendBundle;
use GbsLogistics\Teamster\BackendBundle\DependencyInjection\CompilerPass\DenormalizationRoutinePass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;


/**
 * @author Querns <querns@gbs.io>
 */
class GbsLogisticsTeamsterBackendBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new DenormalizationRoutinePass());
    }

} 
