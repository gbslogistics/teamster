<?php

/** @noinspection SpellCheckingInspection */
namespace GbsLogistics\Teamster\BackendBundle;
use GbsLogistics\Doramad\Client;
use GbsLogistics\Doramad\CrestException;
use GbsLogistics\Doramad\Domain\CrestResourceInterface;
use JMS\Serializer\SerializerInterface;


/**
 * Grabs resources from the Carbon REST API. Uses URIs relative to an injected
 * hostname, to allow the client to work on either TQ or SISI.
 *
 * @author Querns <querns@gbs.io>
 */
class CrestClient extends Client
{
    /** @var string */
    private $hostname;

    /**
     * @param array $config
     * @param SerializerInterface $serializer
     * @param string $hostname
     */
    function __construct(array $config = [], SerializerInterface $serializer, $hostname)
    {
        $this->hostname = $hostname;
        parent::__construct($config, $serializer);
    }

    /**
     * @param $path
     * @return array|CrestResourceInterface
     * @throws CrestException
     */
    public function loadCrestResource($path)
    {
        return parent::loadCrestResource($this->hostname . '/' . $path);
    }

    /**
     * @return string
     */
    public static function getDefaultUserAgent()
    {
        return 'Teamster :: Displays information about Eve: Online industry teams :: http://teamster.gbs.io/ :: Contact <querns@gbs.io>';
    }
}
