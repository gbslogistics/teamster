<?php


namespace GbsLogistics\Teamster\DocumentBundle\Tests\Database;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustrySpeciality;


/**
 * @author Querns <querns@gbs.io>
 * @group database
 */
class IndustrySpecialityTest extends BaseDatabaseTestCase
{
    public function testGetIndustrySpeciality()
    {
        $speciality = new IndustrySpeciality();
        $speciality->setName('speciality');
        $speciality->setSpecializationId(12345);
        $speciality->setGroups([1, 2, 3]);

        $this->dm->persist($speciality);
        $this->dm->flush($speciality);

        $specialityId = $speciality->getId();
        $this->dm->detach($speciality);

        $retrievedSpeciality = $this->dm->getRepository(IndustrySpeciality::class)->find($specialityId);
        $this->assertEquals($speciality, $retrievedSpeciality);
    }
}
