<?php

namespace GbsLogistics\Teamster\DocumentBundle\Tests\Database;
use GbsLogistics\Teamster\DocumentBundle\Document\Character;
use GbsLogistics\Teamster\DocumentBundle\Document\CharacterAffiliation;
use GbsLogistics\Teamster\DocumentBundle\Document\CharacterBid;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustryTeam;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystem;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemBid;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemGeography;
use GbsLogistics\Teamster\DocumentBundle\Document\Worker;
use GbsLogistics\Teamster\DocumentBundle\Document\WorkerBonus;
use GbsLogistics\Teamster\DocumentBundle\Tests\TestFixtures;


/**
 * @author Querns <querns@gbs.io>
 * @group database
 */
class IndustryTeamTest extends BaseDatabaseTestCase
{
    public function testGetIndustryTeam()
    {
        $team = TestFixtures::getTeam();

        $this->dm->persist($team);
        $this->dm->flush($team);

        $teamId = $team->getId();
        $this->dm->detach($team);

        $retrievedTeam = $this->dm->getRepository(get_class($team))->find($teamId);
        $this->assertEquals($team->getTeamId(), $retrievedTeam->getTeamId());
        $this->assertEquals($team->getSolarSystem(), $retrievedTeam->getSolarSystem());
        $this->assertEquals(count($team->getSolarSystemBids()), count($retrievedTeam->getSolarSystemBids()));
        $this->assertEquals(count($team->getWorkers()), count($retrievedTeam->getWorkers()));
        /** @var SolarSystemBid $solarSystemBid */
        $solarSystemBid = $team->getSolarSystemBids()[0];
        /** @var CharacterBid $characterBid */
        $characterBid = $solarSystemBid->getCharacterBids()[0];
        $this->assertInstanceOf(CharacterAffiliation::class, $characterBid->getCharacterAffiliation());
        /** @var WorkerBonus $bonus */
        $bonus = $team->getWorkers()[0]->getBonus();
        $this->assertGreaterThan(0, strlen($bonus->getBonusIdString()));
    }
}

