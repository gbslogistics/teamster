<?php


namespace GbsLogistics\Teamster\DocumentBundle\Tests\Database;


use GbsLogistics\Teamster\DocumentBundle\Document\Batch;

/** @group database */
class BatchTest extends BaseDatabaseTestCase
{
    public function testGetBatch()
    {
        $batch = new Batch();
        $batch->setBatchNumber(time());
        $batch->setType('batch-type');

        $this->dm->persist($batch);
        $this->dm->flush($batch);
        $batchId = $batch->getId();
        $this->dm->detach($batch);

        $retrievedBatch = $this->dm->getRepository(Batch::class)
            ->find($batchId);

        $this->assertEquals($batch, $retrievedBatch);
    }


}
