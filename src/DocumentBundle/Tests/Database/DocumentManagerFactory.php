<?php

namespace GbsLogistics\Teamster\DocumentBundle\Tests\Database;
use Doctrine\Bundle\MongoDBBundle\Mapping\Driver\YamlDriver;
use Doctrine\MongoDB\Connection;
use Doctrine\ODM\MongoDB\Configuration;
use Doctrine\ODM\MongoDB\DocumentManager;


/**
 * @author Querns <querns@gbs.io>
 */
class DocumentManagerFactory
{
    public static function create()
    {
        $tempdir = sys_get_temp_dir() . '/my_5s_teamster_database_tests';
        $proxyDir = $tempdir . '/Proxies';
        $hydratorDir = $tempdir . '/Hydrators';

        if (!file_exists($proxyDir)) {
            mkdir($proxyDir, 0777, true);
        }

        if (!file_exists($hydratorDir)) {
            mkdir($hydratorDir, 0777, true);
        }

        $connection = new Connection();

        $config = new Configuration();
        $config->setProxyDir($proxyDir);
        $config->setProxyNamespace('Proxies');
        $config->setHydratorDir($hydratorDir);
        $config->setHydratorNamespace('Hydrators');
        $config->setDefaultDB('teamster_test');
        $config->setMetadataDriverImpl(new YamlDriver([
            __DIR__ . '/../../Resources/config/doctrine' => 'GbsLogistics\\Teamster\\DocumentBundle\\Document'
        ]));

        return DocumentManager::create($connection, $config);

    }
}
