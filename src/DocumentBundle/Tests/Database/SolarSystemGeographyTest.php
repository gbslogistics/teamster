<?php


namespace GbsLogistics\Teamster\DocumentBundle\Tests\Database;


use GbsLogistics\Teamster\DocumentBundle\Document\IndustryTeam;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemGeography;

/**
 * Class SolarSystemGeographyTest
 * @package GbsLogistics\Teamster\DocumentBundle\Tests\Database
 * @group database
 */
class SolarSystemGeographyTest extends BaseDatabaseTestCase
{
    const CONSTELLATION_ID = 12345;
    const REGION_ID = 45645;
    const FACILITY_TYPE_ONE = 1;
    const FACILITY_TYPE_TWO = 2;
    const WORMHOLE_CLASS = 1;

    const FACTION_ID = 54321;

    const SECURITY = -0.1;

    public function testGetSolarSystemGeography()
    {
        $sut = new SolarSystemGeography();
        $sut->setConstellationId(self::CONSTELLATION_ID);
        $sut->setRegionId(self::REGION_ID);
        $sut->setFacilityTypes([self::FACILITY_TYPE_ONE, self::FACILITY_TYPE_TWO]);
        $sut->setWormholeClass(self::WORMHOLE_CLASS);
        $sut->setFactionId(self::FACTION_ID);
        $sut->setSecurity(self::SECURITY);
        $team = new IndustryTeam();
        $team->setSolarSystemGeography($sut);
        $team->setActivityName('Manufacturing');

        $this->dm->persist($team);
        $this->dm->flush($team);

        $teamId = $team->getId();
        $this->dm->clear();

        $retrievedTeam = $this->dm->find(IndustryTeam::class, $teamId);
        $retrievedGeography = $retrievedTeam->getSolarSystemGeography();

        $this->assertEquals(self::CONSTELLATION_ID, $retrievedGeography->getConstellationId());
        $this->assertEquals(self::REGION_ID, $retrievedGeography->getRegionId());
        $this->assertEquals([self::FACILITY_TYPE_ONE, self::FACILITY_TYPE_TWO], $retrievedGeography->getFacilityTypes());
        $this->assertEquals(self::WORMHOLE_CLASS, $retrievedGeography->getWormholeClass());
        $this->assertEquals(self::SECURITY, $retrievedGeography->getSecurity());
        $this->assertEquals(self::FACTION_ID, $retrievedGeography->getFactionId());
    }
}
 