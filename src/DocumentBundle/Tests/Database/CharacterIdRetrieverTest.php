<?php


namespace GbsLogistics\Teamster\DocumentBundle\Tests\Database;

use Doctrine\ODM\MongoDB\DocumentManager;
use GbsLogistics\Teamster\DocumentBundle\Document\CharacterBid;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustryTeam;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemBid;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @group disabled
 */
class CharacterIdRetrieverTest extends WebTestCase
{
    /** @var DocumentManager */
    private $dm;

    public function setUp()
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $this->dm = $container->get('doctrine.odm.mongodb.document_manager');
    }

    public function testRetrieveCharacterIDs()
    {
        $industryTeams = $this->dm->getRepository(IndustryTeam::class)->findAll();
        $characterIds = [];

        /** @var IndustryTeam $team */
        foreach ($industryTeams as $team) {
            $solarSystemBids = $team->getSolarSystemBids();
            /** @var SolarSystemBid $solarSystemBid */
            foreach ($solarSystemBids as $solarSystemBid) {
                /** @var CharacterBid $characterBid */
                foreach ($solarSystemBid->getCharacterBids() as $characterBid) {
                    $characterIds[] = $characterBid->getCharacter()->getCharacterId();
                }
            }
        }
        $characterIds = array_unique($characterIds);
        echo count($characterIds), "\n", implode(',', $characterIds);
    }
}
 