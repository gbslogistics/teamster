<?php

namespace GbsLogistics\Teamster\DocumentBundle\Tests\Database;
use GbsLogistics\Teamster\BackendBundle\CrestClient;
use GuzzleHttp\Subscriber\Mock;
use Doctrine\ODM\MongoDB\DocumentManager;


/**
 * @author Querns <querns@gbs.io>
 */
class BaseDatabaseTestCase extends \PHPUnit_Framework_TestCase
{
    /** @var DocumentManager */
    protected $dm;

    public function setUp()
    {
        $this->dm = DocumentManagerFactory::create();
        $this->dm->getSchemaManager()->dropCollections();
        $this->dm->getSchemaManager()->createCollections();
    }

	protected function attachMockResponseToClient(CrestClient $crestClient, $responseFile, $headerFile)
    {
        $mockResponse = file_get_contents($responseFile);
        $decodedResponse = json_decode($mockResponse, true);

        $crestClient->getEmitter()->attach(new Mock([
			(null === $headerFile ? '' : file_get_contents($headerFile)) . $mockResponse
        ]));

        if (isset($decodedResponse['totalCount'])) {
            $totalNumberOfExpectedRecords = $decodedResponse['totalCount'];
            $this->assertNotEmpty($totalNumberOfExpectedRecords);

            return $totalNumberOfExpectedRecords;
        }

        return null;
    }
}

