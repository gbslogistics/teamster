<?php


namespace GbsLogistics\Teamster\DocumentBundle\Tests\Database\Repository;


use GbsLogistics\Teamster\DocumentBundle\Document\ConquerableStation;
use GbsLogistics\Teamster\DocumentBundle\Document\Repository\ConquerableStationRepository;
use GbsLogistics\Teamster\DocumentBundle\Tests\Database\BaseDatabaseTestCase;

/** @group database */
class ConquerableStationRepositoryTest extends BaseDatabaseTestCase
{
    const SOLAR_SYSTEM_ID = 30002907;
    const CORPORATION_ID = 12345;
    const STATION_ID = 384765;
    const STATION_TYPE_ID = 48576;

    /** @var ConquerableStationRepository */
    private $repository;

    public function setUp()
    {
        parent::setUp();

        $this->repository = $this->dm->getRepository(ConquerableStation::class);
    }

    public function testRepositoryType()
    {
        $this->assertInstanceOf(ConquerableStationRepository::class, $this->repository);
    }

    public function testFetchBySolarSystemIdsIndexed()
    {
        $station = new ConquerableStation();
        $station->setSolarSystemID(self::SOLAR_SYSTEM_ID);
        $station->setCorporationID(self::CORPORATION_ID);
        $station->setCorporationName('GBS Logistics and Fives Support');
        $station->setStationID(self::STATION_ID);
        $station->setStationTypeID(self::STATION_TYPE_ID);

        $this->dm->persist($station);
        $this->dm->flush();

        $this->dm->clear();

        $results = $this->repository->fetchBySolarSystemIdsIndexed([self::SOLAR_SYSTEM_ID]);

        $this->assertArrayHasKey(self::SOLAR_SYSTEM_ID, $results);
        $this->assertInstanceOf(ConquerableStation::class, $results[self::SOLAR_SYSTEM_ID]);
    }
}
 