<?php


namespace GbsLogistics\Teamster\DocumentBundle\Tests\Database\Repository;


use GbsLogistics\Teamster\DocumentBundle\Document\Batch;
use GbsLogistics\Teamster\DocumentBundle\Document\Repository\BatchRepository;
use GbsLogistics\Teamster\DocumentBundle\Tests\Database\BaseDatabaseTestCase;

/** @group database */
class BatchRepositoryTest extends BaseDatabaseTestCase
{
    const BATCH_TYPE = 'batch-type';

    /** @var BatchRepository */
    private $repository;

    public function setUp()
    {
        parent::setUp();
        $this->repository = $this->dm->getRepository(Batch::class);
    }

    public function testBatchRepositoryType()
    {
        $this->assertInstanceOf(BatchRepository::class, $this->repository);
    }

    public function testGetLastBatch()
    {
        $maxBatchNumber = 10;
        for ($i = $maxBatchNumber; $i >= 0; $i--) {
            $batch = new Batch();
            $batch->setBatchNumber($i);
            $batch->setType(self::BATCH_TYPE);

            $this->dm->persist($batch);
        }

        $otherBatch = new Batch();
        $otherBatch->setBatchNumber($maxBatchNumber + 10);
        $otherBatch->setType('other-batch');
        $this->dm->persist($otherBatch);

        $this->dm->flush();
        $this->dm->clear(Batch::class);

        /** @var Batch $retrievedBatch */
        $retrievedBatch = $this->repository->getLastBatch(self::BATCH_TYPE);
        $this->assertEquals($maxBatchNumber, $retrievedBatch->getBatchNumber());
    }
}
