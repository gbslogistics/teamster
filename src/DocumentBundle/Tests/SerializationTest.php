<?php

namespace GbsLogistics\Teamster\DocumentBundle\Tests;

use GbsLogistics\Teamster\DocumentBundle\Document\Character;
use GbsLogistics\Teamster\DocumentBundle\Document\CharacterAffiliation;
use GbsLogistics\Teamster\DocumentBundle\Document\CharacterBid;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustrySpeciality;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustryTeam;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystem;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemBid;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemGeography;
use GbsLogistics\Teamster\DocumentBundle\Document\Worker;
use GbsLogistics\Teamster\DocumentBundle\Document\WorkerBonus;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;

class SerializationTest extends \PHPUnit_Framework_TestCase
{
    /** @var SerializerInterface */
    protected $serializer;

    public function setUp()
    {
        $this->serializer = SerializerBuilder::create()
            ->addMetadataDir(
                __DIR__ . '/../Resources/config/serializer/document',
                'GbsLogistics\Teamster\DocumentBundle\Document'
            )
            ->addMetadataDir(
                __DIR__ . '/../Resources/config/serializer/domain',
                'GbsLogistics\Doramad\Domain'
            )
            ->build();
    }

    public function testSerializeCharacter()
    {
        /** @var Character $character */
        $character = $this->performSerializationTestAndReturn(TestFixtures::getCharacter(), Character::class);
        $this->assertNotNull($character->getCharacterId());
        $this->assertNotNull($character->getIsNpc());
        $this->assertNotNull($character->getName());
    }

    public function testSerializeCharacterBid()
    {
        /** @var CharacterBid $bid */
        $bid = $this->performSerializationTestAndReturn(TestFixtures::getCharacterBid(), CharacterBid::class);
        $this->assertNotNull($bid->getCharacterAffiliation());
        $this->assertNotNull($bid->getCharacter());
        $this->assertNotNull($bid->getBidAmount());
    }

    public function testSerializeCharacterAffiliation()
    {
        $this->performSerializationTestAndReturn(
            TestFixtures::getCharacterAffiliation(),
            CharacterAffiliation::class
        );
    }

    public function testSerializeSolarSystem()
    {
        $this->performSerializationTestAndReturn(
            TestFixtures::getSolarSystem(),
            SolarSystem::class
        );
    }

    public function testSerializeSolarSystemBid()
    {
        /** @var SolarSystemBid $bid */
        $bid = $this->performSerializationTestAndReturn(
            TestFixtures::getSolarSystemBid(),
            SolarSystemBid::class
        );

        $this->assertNotNull($bid->getSolarSystem());
        $this->assertNotNull($bid->getSolarSystemGeography());
    }

    public function testSerializeSolarSystemGeography()
    {
        $this->performSerializationTestAndReturn(
            TestFixtures::getSolarSystemGeography(),
            SolarSystemGeography::class
        );
    }

    public function testSerializeWorker()
    {
        /** @var Worker $worker */
        $worker = $this->performSerializationTestAndReturn(
            TestFixtures::getWorker(),
            Worker::class
        );

        $this->assertNotNull($worker->getBonus());
    }

    public function testSerializeWorkerBonus()
    {
        $this->performSerializationTestAndReturn(
            TestFixtures::getWorkerBonus(),
            WorkerBonus::class
        );
    }

    public function testSerializeIndustryTeam()
    {
        $industryTeam = TestFixtures::getTeam();
        $this->performSerializationTestAndReturn(
            $industryTeam,
            IndustryTeam::class
        );
    }

    public function testSerializeIndustrySpecialty()
    {
        $speciality = TestFixtures::getSpeciality();
        $this->performSerializationTestAndReturn($speciality, IndustrySpeciality::class);
    }

    private function performSerializationTestAndReturn($object, $class, $format = 'json')
    {
        $serializedData = $this->serializer->serialize($object, $format);
        $returnedObject = $this->serializer->deserialize($serializedData, $class, $format);
        $this->assertEquals($object, $returnedObject);

        return $returnedObject;
    }
}
 