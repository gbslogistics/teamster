<?php

namespace GbsLogistics\Teamster\DocumentBundle\Tests;


use GbsLogistics\Doramad\Domain\IndustrySpeciality;
use GbsLogistics\Doramad\Domain\IndustryTeam;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustrySpeciality as DocumentSpeciality;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustryTeam as DcoumentTeam;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemBid;
use GbsLogistics\Teamster\DocumentBundle\DomainMapper;

class DomainMapperTest extends \PHPUnit_Framework_TestCase
{
    /** @var DomainMapper */
    private $sut;

    public function setUp()
    {
        $this->sut = new DomainMapper();
    }

    public function testSolarSystemBidInformationIsNeverOverwritten()
    {
        $domainTeam = new IndustryTeam();

        $documentTeam = new DcoumentTeam();
        $documentTeam->setSolarSystemBids([ new SolarSystemBid() ]);
        $this->sut->map($domainTeam, $documentTeam);

        $this->assertCount(1, $documentTeam->getSolarSystemBids());
    }

	public function testIndustrySpecialityMapping()
	{
		$domainSpeciality = new IndustrySpeciality();
		$domainSpeciality->setGroups([1]);
		$domainSpeciality->setName('GBS Team GBS00');
		$domainSpeciality->setSpecializationId(555);
		$documentSpeciality = new DocumentSpeciality();

		$this->sut->map($domainSpeciality, $documentSpeciality);

		$this->assertEquals($domainSpeciality->getName(), $documentSpeciality->getName());
		$this->assertEquals($domainSpeciality->getGroups(), $documentSpeciality->getGroups());
		$this->assertEquals($domainSpeciality->getSpecializationId(), $documentSpeciality->getSpecializationId());
	}
}
 