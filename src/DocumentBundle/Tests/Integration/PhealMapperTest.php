<?php
namespace GbsLogistics\Teamster\DocumentBundle\Tests\Integration;

use GbsLogistics\Teamster\DocumentBundle\Document\ConquerableStation;
use GbsLogistics\Teamster\DocumentBundle\PhealMapper;
use Pheal\Pheal;

/** @group integration */
class PhealMapperTest extends \PHPUnit_Framework_TestCase
{
    /** @var PhealMapper */
    private $sut;

    /** @var Pheal */
    private $pheal;

    public function setUp()
    {
        $this->sut = new PhealMapper();
        $this->pheal = new Pheal();
    }

    public function testMapPhealConquerableStationResults()
    {
        $results = $this->pheal->eveScope->ConquerableStationList();

        foreach ($results->outposts as $result) {
            $station = new ConquerableStation();
            $this->sut->map($result, $station);

            $this->assertNotNull($station->getCorporationID());
            $this->assertNotNull($station->getCorporationName());
            $this->assertNotNull($station->getSolarSystemID());
            $this->assertNotNull($station->getStationID());
            $this->assertNotNull($station->getStationName());
            $this->assertNotNull($station->getStationTypeID());
        }
    }
}
 