<?php

namespace GbsLogistics\Teamster\DocumentBundle\Tests\Domain;

use GbsLogistics\Teamster\DocumentBundle\Model\Proxy;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustryTeam;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystem;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemGeography;

class IndustryTeamTest extends \PHPUnit_Framework_TestCase
{
    const SOLAR_SYSTEM_ID = 12345;

    /** @var IndustryTeam */
    private $sut;

    public function setUp()
    {
        $this->sut = new IndustryTeam();
    }

    public function testSetSolarSystemOverwritesGeography()
    {
        $solarSystem = new SolarSystem();
        $solarSystem->setSolarSystemId(self::SOLAR_SYSTEM_ID);
        $this->sut->setSolarSystemGeography(new SolarSystemGeography());
        $this->sut->setSolarSystem($solarSystem);
        $this->assertInstanceOf(SolarSystemGeography::class, $this->sut->getSolarSystemGeography());

        $newSolarSystem = new SolarSystem();
        $newSolarSystem->setSolarSystemId(53431);
        $this->sut->setSolarSystem($newSolarSystem);

        $this->assertInstanceOf(Proxy::class, $this->sut->getSolarSystemGeography());
    }
}
 