<?php

namespace GbsLogistics\Teamster\DocumentBundle\Tests\Functional;

use GbsLogistics\Teamster\DocumentBundle\Document\IndustryTeam;
use GbsLogistics\Teamster\DocumentBundle\Tests\TestFixtures;
use JMS\Serializer\Serializer;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class SerializationTest
 * @package GbsLogistics\Teamster\DocumentBundle\Tests\Functional
 * @group functional
 */
class SerializationTest extends WebTestCase
{
    const SERIALIZATION_FORMAT = 'json';
    /** @var Serializer */
    private $serializer;

    public function setUp()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $this->serializer = $container->get('jms_serializer');
    }

    public function testSerializationOfIndustryTeam()
    {
        $team = TestFixtures::getTeam();
        $json = $this->serializer->serialize($team, self::SERIALIZATION_FORMAT);
        $outputTeam = $this->serializer->deserialize($json, IndustryTeam::class, self::SERIALIZATION_FORMAT);

        $this->assertEquals($team, $outputTeam);
        $parsedJson = json_decode($json, true);
        $this->assertArrayHasKey('teamId', $parsedJson);
    }
}
 