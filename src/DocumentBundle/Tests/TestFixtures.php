<?php

namespace GbsLogistics\Teamster\DocumentBundle\Tests;


use GbsLogistics\Teamster\DocumentBundle\Document\Character;
use GbsLogistics\Teamster\DocumentBundle\Document\CharacterAffiliation;
use GbsLogistics\Teamster\DocumentBundle\Document\CharacterBid;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustrySpeciality;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustryTeam;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystem;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemBid;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemGeography;
use GbsLogistics\Teamster\DocumentBundle\Document\Worker;
use GbsLogistics\Teamster\DocumentBundle\Document\WorkerBonus;

class TestFixtures
{
    /**
     * @return IndustryTeam
     */
    public static function getTeam()
    {
        $team = new IndustryTeam();
        $date = new \DateTime();
        $team->setActivityId(1);
        $team->setAuctionExpiryTime($date);
        $team->setCostModifier(2.0);
        $team->setCreationTime($date);
        $team->setExpiryTime($date);
        $team->setName('team');
        $team->setSolarSystem(self::getSolarSystem());
        $team->setSolarSystemBids([self::getSolarSystemBid()]);
        $team->setSpecializationId(1);
        $team->setTeamId(1);
        $team->setWorkers([self::getWorker()]);
        $team->setSolarSystemGeography(self::getSolarSystemGeography());
        $team->setActivityName('Manufacturing');

        return $team;
    }

    /**
     * @return Character
     */
    public static function getCharacter()
    {
        $character = new Character();
        /** @noinspection SpellCheckingInspection */
        $character->setName('Jita Man');
        $character->setCharacterId(1);
        $character->setIsNpc(false);
        return $character;
    }

    /**
     * @return CharacterAffiliation
     */
    public static function getCharacterAffiliation()
    {
        $characterAffiliation = new CharacterAffiliation();
        $characterAffiliation->setAllianceID(129837);
        $characterAffiliation->setAllianceName('Alliance');
        $characterAffiliation->setCharacterID(465);
        $characterAffiliation->setCharacterName('Character name');
        $characterAffiliation->setCorporationID(8327462);
        $characterAffiliation->setCorporationName('Corporation name');
        $characterAffiliation->setFactionID(128937);
        $characterAffiliation->setFactionName('Faction name');

        return $characterAffiliation;
    }

    /**
     * @return CharacterBid
     */
    public static function getCharacterBid()
    {
        $characterBid = new CharacterBid();
        $characterBid->setBidAmount(123);
        $characterBid->setCharacter(self::getCharacter());
        $characterBid->setCharacterAffiliation(self::getCharacterAffiliation());
        return $characterBid;
    }

    /**
     * @return SolarSystem
     */
    public static function getSolarSystem()
    {
        /** @noinspection SpellCheckingInspection */
        $solarSystem = new SolarSystem();
        $solarSystem->setName('Jita');
        $solarSystem->setSolarSystemId('1');
        return $solarSystem;
    }

    /**
     * @return SolarSystemBid
     */
    public static function getSolarSystemBid()
    {
        $solarSystem = self::getSolarSystem();
        $characterBid = self::getCharacterBid();
        $solarSystemBid = new SolarSystemBid();
        $bidGeography = new SolarSystemGeography();
        $bidGeography->setWormholeClass(null);
        $bidGeography->setConstellationId(127635);
        $bidGeography->setConstellationName('Constellation for bid');
        $bidGeography->setFacilityTypes([1]);
        $bidGeography->setRegionId(27836);
        $bidGeography->setRegionName('Region for bid');

        $solarSystemBid->setBidAmount(123);
        $solarSystemBid->setSolarSystem($solarSystem);
        $solarSystemBid->setCharacterBids([$characterBid]);
        $solarSystemBid->setSolarSystemGeography($bidGeography);
        return $solarSystemBid;
    }

    /**
     * @return SolarSystemGeography
     */
    public static function getSolarSystemGeography()
    {
        $geography = new SolarSystemGeography();
        $geography->setWormholeClass(1);
        $geography->setConstellationId(891273172);
        $geography->setConstellationName('Constellation for team');
        $geography->setFacilityTypes([2]);
        $geography->setRegionId(27889234932);
        $geography->setRegionName('Region for team');
        $geography->setSecurity(0.1);
        $geography->setFactionId(12345);
        return $geography;
    }

    /**
     * @return WorkerBonus
     */
    public static function getWorkerBonus()
    {
        $bonus = new WorkerBonus();
        $bonus->setBonusId(1);
        $bonus->setBonusType(-1);
        $bonus->setBonusIdString('ME');
        $bonus->setValue(1);
        return $bonus;
    }

    /**
     * @return Worker
     */
    public static function getWorker()
    {
        $worker = new Worker();
        $worker->setSpecializationId(1);
        $worker->setBonus(self::getWorkerBonus());
        return $worker;
    }

    /**
     * @return IndustrySpeciality
     */
    public static function getSpeciality()
    {
        $speciality = new IndustrySpeciality();
        $speciality->setGroups([1, 2]);
        $speciality->setName('Test Speciality');
        $speciality->setSpecializationId(123);
        return $speciality;
    }
}
