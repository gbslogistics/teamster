<?php

namespace GbsLogistics\Teamster\DocumentBundle;


use BCC\AutoMapperBundle\Mapper\Mapper;
use GbsLogistics\Teamster\DocumentBundle\Map\CharacterAffiliationMap;
use GbsLogistics\Teamster\DocumentBundle\Map\ConquerableStationMap;

class PhealMapper extends Mapper
{

    function __construct()
    {
        $this->registerMap(new ConquerableStationMap());
        $this->registerMap(new CharacterAffiliationMap());
    }
}