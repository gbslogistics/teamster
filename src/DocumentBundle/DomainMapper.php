<?php

namespace GbsLogistics\Teamster\DocumentBundle;


use BCC\AutoMapperBundle\Mapper\Mapper;
use GbsLogistics\Teamster\DocumentBundle\Map\CharacterBidMap;
use GbsLogistics\Teamster\DocumentBundle\Map\CharacterMap;
use GbsLogistics\Teamster\DocumentBundle\Map\IndustrySpecialityMap;
use GbsLogistics\Teamster\DocumentBundle\Map\IndustryTeamMap;
use GbsLogistics\Teamster\DocumentBundle\Map\SolarSystemBidMap;
use GbsLogistics\Teamster\DocumentBundle\Map\SolarSystemMap;
use GbsLogistics\Teamster\DocumentBundle\Map\WorkerBonusMap;
use GbsLogistics\Teamster\DocumentBundle\Map\WorkerMap;

class DomainMapper extends Mapper
{
    function __construct()
    {
        $this->registerMap(new IndustryTeamMap($this));
        $this->registerMap(new SolarSystemMap());
        $this->registerMap(new WorkerBonusMap());
        $this->registerMap(new WorkerMap($this));
        $this->registerMap(new SolarSystemBidMap($this));
        $this->registerMap(new CharacterBidMap($this));
        $this->registerMap(new CharacterMap());
		$this->registerMap(new IndustrySpecialityMap());
    }
}

