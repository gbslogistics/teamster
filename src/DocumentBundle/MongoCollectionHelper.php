<?php

namespace GbsLogistics\Teamster\DocumentBundle;
use Doctrine\ODM\MongoDB\DocumentManager;


/**
 * @author Querns <querns@gbs.io>
 */
class MongoCollectionHelper
{
	/** @var DocumentManager */
	private $dm;

	function __construct(DocumentManager $dm)
	{
		$this->dm = $dm;
	}

	/**
	 * @param $documentName
	 * @throws \Doctrine\ODM\MongoDB\MongoDBException
	 */
	public function purgeMongoCollection($documentName)
	{
		$qb = $this->dm->createQueryBuilder($documentName);
		$qb->remove()->getQuery()->execute();
	}
}
