<?php

namespace GbsLogistics\Teamster\DocumentBundle;
use Symfony\Component\HttpKernel\Bundle\Bundle;


/**
 * @author Querns <querns@gbs.io>
 */
class GbsLogisticsTeamsterDocumentBundle extends Bundle
{

} 
