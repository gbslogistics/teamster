<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document;

/**
 * Gives affected classes a batch field.
 *
 * Class BatchAware
 * @package GbsLogistics\Teamster\DocumentBundle\Document
 */
trait BatchAware
{
    /** @var integer */
    private $batchNumber;

    /**
     * @param int $batchNumber
     */
    public function setBatchNumber($batchNumber)
    {
        $this->batchNumber = $batchNumber;
    }

    /**
     * @return int
     */
    public function getBatchNumber()
    {
        return $this->batchNumber;
    }

}

