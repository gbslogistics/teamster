<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document;


trait DiscriminatorTrait
{
    /** @var mixed */
    protected $discriminator;

    /**
     * @return mixed
     */
    public function getDiscriminator()
    {
        return $this->discriminator;
    }

    /**
     * @param mixed $discriminator
     */
    public function setDiscriminator($discriminator)
    {
        $this->discriminator = $discriminator;
    }

} 