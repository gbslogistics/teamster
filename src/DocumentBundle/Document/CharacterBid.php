<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document;
use GbsLogistics\Teamster\DocumentBundle\Model\Proxy;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Routine\CharacterAffiliationRoutine;

/**
 * @author Querns <querns@gbs.io>
 */
class CharacterBid extends \GbsLogistics\Doramad\Domain\IndustryTeam\CharacterBid
{
    /** @var CharacterAffiliation|\GbsLogistics\Teamster\DocumentBundle\Model\Proxy */
    private $characterAffiliation;

    function __construct()
    {
        parent::__construct();
        $this->setCharacterAffiliation(new Proxy(
            CharacterAffiliationRoutine::class,
            function () {
                return $this->getCharacter()->getCharacterId();
            },
            function (CharacterAffiliation $result) {
                $this->setCharacterAffiliation($result);
            }
        ));
    }


    /**
     * @return CharacterAffiliation|\GbsLogistics\Teamster\DocumentBundle\Model\Proxy
     */
    public function getCharacterAffiliation()
    {
        return $this->characterAffiliation;
    }

    /**
     * @param CharacterAffiliation|Proxy $characterAffiliation
     */
    public function setCharacterAffiliation($characterAffiliation)
    {
        $this->characterAffiliation = $characterAffiliation;
    }


}
