<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document;
use GbsLogistics\Teamster\DocumentBundle\Model\Proxy;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Routine\WorkerBonusTypeRoutine;
use GbsLogistics\Teamster\BackendBundle\Model\WorkerBonusTypeRoutineResult;


/**
 * @author Querns <querns@gbs.io>
 */
class WorkerBonus extends \GbsLogistics\Doramad\Domain\IndustryTeam\WorkerBonus
{
    /**
     * @var \GbsLogistics\Teamster\DocumentBundle\Model\Proxy|string
     */
    private $bonusIdString;

    function __construct()
    {
        $this->bonusIdString = new Proxy(
            WorkerBonusTypeRoutine::class,
            function () {
                return $this->getBonusId();
            },
            function (WorkerBonusTypeRoutineResult $result) {
                $this->setBonusIdString($result->getBonusIdString());
            }
        );
    }

    /**
     * @return \GbsLogistics\Teamster\DocumentBundle\Model\Proxy|string
     */
    public function getBonusIdString()
    {
        return $this->bonusIdString;
    }

    /**
     * @param \GbsLogistics\Teamster\DocumentBundle\Model\Proxy|string $bonusIdString
     */
    public function setBonusIdString($bonusIdString)
    {
        $this->bonusIdString = $bonusIdString;
    }
}
