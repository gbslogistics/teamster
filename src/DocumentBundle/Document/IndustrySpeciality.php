<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document;


/**
 * @author Querns <querns@gbs.io>
 */
class IndustrySpeciality extends \GbsLogistics\Doramad\Domain\IndustrySpeciality
{
    use BatchAware;
}

