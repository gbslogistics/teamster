<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document;


/**
 * Represents a distinct loading event performed by Teamster.
 *
 * @author Querns <querns@gbs.io>
 */
class Batch
{
    /** @var string */
    private $id;

    /** @var int */
    private $batchNumber;

    /** @var string */
    private $type;

	/** @var \DateTime */
	private $dateCreated;

	function __construct()
	{
		$this->dateCreated = new \DateTime();
	}

	/**
     * @param int $batchNumber
     */
    public function setBatchNumber($batchNumber)
    {
        $this->batchNumber = $batchNumber;
    }

    /**
     * @return int
     */
    public function getBatchNumber()
    {
        return $this->batchNumber;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

	/**
	 * @return \DateTime
	 */
	public function getDateCreated()
	{
		return $this->dateCreated;
	}

	/**
	 * @param \DateTime $dateCreated
	 */
	public function setDateCreated(\DateTime $dateCreated)
	{
		$this->dateCreated = $dateCreated;
	}

}

