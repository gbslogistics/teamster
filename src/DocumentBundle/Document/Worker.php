<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document;


/**
 * @author Querns <querns@gbs.io>
 */
class Worker extends \GbsLogistics\Doramad\Domain\IndustryTeam\Worker
{
    function __construct()
    {
        parent::__construct();

        $this->bonus = new WorkerBonus;
    }
}
