<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document;
use GbsLogistics\Teamster\DocumentBundle\Model\Proxy;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Routine\SolarSystemGeographyRoutine;


/**
 * @author Querns <querns@gbs.io>
 */
class SolarSystemBid extends \GbsLogistics\Doramad\Domain\IndustryTeam\SolarSystemBid
{
    /** @var SolarSystemGeography|Proxy */
    private $solarSystemGeography;

    function __construct()
    {
        parent::__construct();

        $this->solarSystem = new SolarSystem();
        $this->solarSystemGeography = new Proxy(
            SolarSystemGeographyRoutine::class,
            function () {
                return $this->getSolarSystem()->getSolarSystemId();
            },
            function (SolarSystemGeography $geography) {
                $this->setSolarSystemGeography($geography);
            }
        );
    }

    /**
     * @return Proxy|SolarSystemGeography
     */
    public function getSolarSystemGeography()
    {
        return $this->solarSystemGeography;
    }

    /**
     * @param Proxy|SolarSystemGeography $solarSystemGeography
     */
    public function setSolarSystemGeography($solarSystemGeography)
    {
        $this->solarSystemGeography = $solarSystemGeography;
    }
}
