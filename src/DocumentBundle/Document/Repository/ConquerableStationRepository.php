<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document\Repository;


use Doctrine\ODM\MongoDB\DocumentRepository;
use GbsLogistics\Teamster\DocumentBundle\Document\ConquerableStation;

class ConquerableStationRepository extends DocumentRepository
{
    public function fetchBySolarSystemIdsIndexed(array $solarSystemIds)
    {

        $results = $this->createQueryBuilder()
            ->field('solarSystemID')->in($solarSystemIds)
            ->getQuery()->execute();
        $resultsIndexed = [];

        /** @var ConquerableStation $result */
        foreach ($results as $result) {
            $resultsIndexed[$result->getSolarSystemID()] = $result;
        }

        return $resultsIndexed;
    }
}
