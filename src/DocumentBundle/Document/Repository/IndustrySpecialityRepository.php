<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document\Repository;


use Doctrine\ODM\MongoDB\DocumentRepository;

class IndustrySpecialityRepository extends DocumentRepository
{
    public function fetchBySpecialtyIds($specializationIds)
    {
        return $this->createQueryBuilder()
            ->field('specializationId')->in($specializationIds)
            ->getQuery()->execute()->toArray(false);
    }
}
