<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class IndustryTeamRepository extends DocumentRepository
{
    public function fetchByTeamIds(array $teamIds)
    {
        return $this->createQueryBuilder()
            ->field('teamId')->in($teamIds)
            ->getQuery()->execute()->toArray(false);
    }
}
