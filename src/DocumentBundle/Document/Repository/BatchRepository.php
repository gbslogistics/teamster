<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document\Repository;
use Doctrine\ODM\MongoDB\DocumentRepository;


/**
 * @author Querns <querns@gbs.io>
 */
class BatchRepository extends DocumentRepository
{
    public function getLastBatch($batchType)
    {
        return $this->createQueryBuilder()
            ->field('type')->equals($batchType)
            ->sort('batchNumber', 'desc')
            ->getQuery()->getSingleResult();
    }
}

