<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document;


class ConquerableStation
{
    private $id;

    /** @var integer */
    private $stationID;

    /** @var string */
    private $stationName;

    /** @var integer */
    private $stationTypeID;

    /** @var integer */
    private $solarSystemID;

    /** @var integer */
    private $corporationID;

    /** @var string */
    private $corporationName;

    /**
     * @return int
     */
    public function getCorporationID()
    {
        return $this->corporationID;
    }

    /**
     * @param int $corporationID
     */
    public function setCorporationID($corporationID)
    {
        $this->corporationID = $corporationID;
    }

    /**
     * @return string
     */
    public function getCorporationName()
    {
        return $this->corporationName;
    }

    /**
     * @param string $corporationName
     */
    public function setCorporationName($corporationName)
    {
        $this->corporationName = $corporationName;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSolarSystemID()
    {
        return $this->solarSystemID;
    }

    /**
     * @param int $solarSystemID
     */
    public function setSolarSystemID($solarSystemID)
    {
        $this->solarSystemID = $solarSystemID;
    }

    /**
     * @return int
     */
    public function getStationID()
    {
        return $this->stationID;
    }

    /**
     * @param int $stationID
     */
    public function setStationID($stationID)
    {
        $this->stationID = $stationID;
    }

    /**
     * @return string
     */
    public function getStationName()
    {
        return $this->stationName;
    }

    /**
     * @param string $stationName
     */
    public function setStationName($stationName)
    {
        $this->stationName = $stationName;
    }

    /**
     * @return int
     */
    public function getStationTypeID()
    {
        return $this->stationTypeID;
    }

    /**
     * @param int $stationTypeID
     */
    public function setStationTypeID($stationTypeID)
    {
        $this->stationTypeID = $stationTypeID;
    }
}

