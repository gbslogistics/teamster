<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document;


class SolarSystemGeography
{
    use DiscriminatorTrait;

    /** @var integer */
    private $constellationId;

    /** @var string */
    private $constellationName;

    /** @var integer */
    private $regionId;

    /** @var string */
    private $regionName;

    /** @var array */
    private $facilityTypes;

    /** @var integer|null */
    private $wormholeClass;

    /** @var float */
    private $security;

    /** @var integer */
    private $factionId;

    /**
     * @return int
     */
    public function getConstellationId()
    {
        return $this->constellationId;
    }

    /**
     * @param int $constellationId
     */
    public function setConstellationId($constellationId)
    {
        $this->constellationId = $constellationId;
    }

    /**
     * @return string
     */
    public function getConstellationName()
    {
        return $this->constellationName;
    }

    /**
     * @param string $constellationName
     */
    public function setConstellationName($constellationName)
    {
        $this->constellationName = $constellationName;
    }

    /**
     * @return array
     */
    public function getFacilityTypes()
    {
        return $this->facilityTypes;
    }

    /**
     * @param array $facilityTypes
     */
    public function setFacilityTypes($facilityTypes)
    {
        $this->facilityTypes = $facilityTypes;
    }

    /**
     * @return int
     */
    public function getRegionId()
    {
        return $this->regionId;
    }

    /**
     * @param int $regionId
     */
    public function setRegionId($regionId)
    {
        $this->regionId = $regionId;
    }

    /**
     * @return string
     */
    public function getRegionName()
    {
        return $this->regionName;
    }

    /**
     * @param string $regionName
     */
    public function setRegionName($regionName)
    {
        $this->regionName = $regionName;
    }

    /**
     * @return int|null
     */
    public function getWormholeClass()
    {
        return $this->wormholeClass;
    }

    /**
     * @param int|null $wormholeClass
     */
    public function setWormholeClass($wormholeClass)
    {
        $this->wormholeClass = $wormholeClass;
    }

    /**
     * @return int
     */
    public function getFactionId()
    {
        return $this->factionId;
    }

    /**
     * @param int $factionId
     */
    public function setFactionId($factionId)
    {
        $this->factionId = $factionId;
    }

    /**
     * @return float
     */
    public function getSecurity()
    {
        return $this->security;
    }

    /**
     * @param float $security
     */
    public function setSecurity($security)
    {
        $this->security = $security;
    }
}
