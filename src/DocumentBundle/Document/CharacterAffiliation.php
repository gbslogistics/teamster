<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document;


class CharacterAffiliation
{
    /** @var integer */
    private $characterID;
    /** @var string */
    private $characterName;
    /** @var integer */
    private $corporationID;
    /** @var string */
    private $corporationName;
    /** @var integer */
    private $allianceID;
    /** @var string */
    private $allianceName;
    /** @var integer */
    private $factionID;
    /** @var string */
    private $factionName;

    /**
     * @return int
     */
    public function getAllianceID()
    {
        return $this->allianceID;
    }

    /**
     * @param int $allianceId
     */
    public function setAllianceID($allianceId)
    {
        $this->allianceID = $allianceId;
    }

    /**
     * @return string
     */
    public function getAllianceName()
    {
        return $this->allianceName;
    }

    /**
     * @param string $allianceName
     */
    public function setAllianceName($allianceName)
    {
        $this->allianceName = $allianceName;
    }

    /**
     * @return int
     */
    public function getCharacterID()
    {
        return $this->characterID;
    }

    /**
     * @param int $characterId
     */
    public function setCharacterID($characterId)
    {
        $this->characterID = $characterId;
    }

    /**
     * @return string
     */
    public function getCharacterName()
    {
        return $this->characterName;
    }

    /**
     * @param string $characterName
     */
    public function setCharacterName($characterName)
    {
        $this->characterName = $characterName;
    }

    /**
     * @return int
     */
    public function getCorporationID()
    {
        return $this->corporationID;
    }

    /**
     * @param int $corporationId
     */
    public function setCorporationID($corporationId)
    {
        $this->corporationID = $corporationId;
    }

    /**
     * @return string
     */
    public function getCorporationName()
    {
        return $this->corporationName;
    }

    /**
     * @param string $corporationName
     */
    public function setCorporationName($corporationName)
    {
        $this->corporationName = $corporationName;
    }

    /**
     * @return int
     */
    public function getFactionID()
    {
        return $this->factionID;
    }

    /**
     * @param int $factionId
     */
    public function setFactionID($factionId)
    {
        $this->factionID = $factionId;
    }

    /**
     * @return string
     */
    public function getFactionName()
    {
        return $this->factionName;
    }

    /**
     * @param string $factionName
     */
    public function setFactionName($factionName)
    {
        $this->factionName = $factionName;
    }
}

