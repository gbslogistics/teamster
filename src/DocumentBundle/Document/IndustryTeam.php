<?php

namespace GbsLogistics\Teamster\DocumentBundle\Document;
use GbsLogistics\SdeEntityBundle\Entity\RamActivity;
use GbsLogistics\Teamster\BackendBundle\Denormalization\DenormalizableEntityInterface;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Routine\IndustryActivityRoutine;
use GbsLogistics\Teamster\DocumentBundle\Model\Proxy;
use GbsLogistics\Teamster\BackendBundle\Denormalization\Routine\SolarSystemGeographyRoutine;


/**
 * @author Querns <querns@gbs.io>
 */
class IndustryTeam extends \GbsLogistics\Doramad\Domain\IndustryTeam implements DenormalizableEntityInterface
{
    use BatchAware;

    /** @var SolarSystemGeography|\GbsLogistics\Teamster\DocumentBundle\Model\Proxy */
    private $solarSystemGeography;

    /** @var string */
    private $activityName;

    function __construct()
    {
        parent::__construct();

        $this->solarSystem = new SolarSystem();
        $this->loadSolarSystemGeographyProxy();
        $this->activityName = new Proxy(
            IndustryActivityRoutine::class,
            function () {
                return $this->getActivityId();
            },
            function (RamActivity $activity) {
                $this->setActivityName($activity->getActivityName());
            }
        );
    }

    public function getCharterTime()
    {
        return $this->getExpiryTime()->sub(new \DateInterval('P28W'));
    }

    /**
     * @return \GbsLogistics\Teamster\DocumentBundle\Model\Proxy|SolarSystemGeography
     */
    public function getSolarSystemGeography()
    {
        return $this->solarSystemGeography;
    }

    /**
     * @param \GbsLogistics\Teamster\DocumentBundle\Model\Proxy|SolarSystemGeography $solarSystemGeography
     */
    public function setSolarSystemGeography($solarSystemGeography)
    {
        $this->solarSystemGeography = $solarSystemGeography;
    }

    /**
     * Requests a list of denormalization proxies from the implementing entity.
     *
     * @return array<DenormalizationProxy>
     */
    public function getDenormalizationProxies()
    {
        $proxies = [
            $this->getSolarSystemGeography(),
            $this->getActivityName(),
        ];

        /** @var SolarSystemBid $bid */
        foreach ($this->getSolarSystemBids() as $bid) {
            $proxies[] = $bid->getSolarSystemGeography();
            /** @var CharacterBid $characterBid */
            foreach ($bid->getCharacterBids() as $characterBid) {
                $proxies[] = $characterBid->getCharacterAffiliation();
            }
        }

        /** @var Worker $worker */
        foreach ($this->getWorkers() as $worker) {
            /** @var WorkerBonus $workerBonus */
            $workerBonus = $worker->getBonus();
            $proxies[] = $workerBonus->getBonusIdString();
        }

        return $proxies;
    }

    public function setSolarSystem($solarSystem)
    {
        $oldSolarSystem = $this->getSolarSystem();
        if (null !== $oldSolarSystem->getSolarSystemId() && null !== $solarSystem->getSolarSystemId()
            && $oldSolarSystem->getSolarSystemId() != $solarSystem->getSolarSystemId()
        ) {
            // We are deliberately overwriting the old value of solar system geography since it changed
            $this->loadSolarSystemGeographyProxy();
        }
        return parent::setSolarSystem($solarSystem);
    }

    public function getName()
    {
        return str_replace('<br>', ' ', parent::getName());
    }

    /**
     * @return string
     */
    public function getActivityName()
    {
        return $this->activityName;
    }

    /**
     * @param string $activityName
     */
    public function setActivityName($activityName)
    {
        $this->activityName = $activityName;
    }

    private function loadSolarSystemGeographyProxy()
    {
        $this->solarSystemGeography = new Proxy(
            SolarSystemGeographyRoutine::class,
            function () {
                return $this->getSolarSystem()->getSolarSystemId();
            },
            function (SolarSystemGeography $geography) {
                $this->setSolarSystemGeography($geography);
            }
        );
    }
}

