<?php

namespace GbsLogistics\Teamster\DocumentBundle\Model;

class Proxy
{
    /** @var string */
    private $routineClass;

    /** @var callback */
    private $discriminatorCallback;

    /** @var callback */
    private $denormalizationCallback;

    function __construct($routineClass, $discriminatorCallback, $denormalizationCallback)
    {
        $this->denormalizationCallback = $denormalizationCallback;
        $this->discriminatorCallback = $discriminatorCallback;
        $this->routineClass = $routineClass;
    }


    /**
     * @return callback
     */
    public function getDiscriminatorCallback()
    {
        return $this->discriminatorCallback;
    }

    /**
     * @param callback $discriminator
     * @return $this
     */
    public function setDiscriminatorCallback($discriminator)
    {
        $this->discriminatorCallback = $discriminator;
        return $this;
    }

    /**
     * @return string
     */
    public function getRoutineClass()
    {
        return $this->routineClass;
    }

    /**
     * @param string $routine
     * @return $this
     */
    public function setRoutineClass($routine)
    {
        $this->routineClass = $routine;
        return $this;
    }

    /**
     * @return callable
     */
    public function getDenormalizationCallback()
    {
        return $this->denormalizationCallback;
    }

    /**
     * @param callable $callback
     * @return $this
     */
    public function setDenormalizationCallback($callback)
    {
        $this->denormalizationCallback = $callback;
        return $this;
    }
}
