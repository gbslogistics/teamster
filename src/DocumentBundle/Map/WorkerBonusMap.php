<?php

namespace GbsLogistics\Teamster\DocumentBundle\Map;


use BCC\AutoMapperBundle\Mapper\AbstractMap;
use GbsLogistics\Doramad\Domain\IndustryTeam\WorkerBonus;

class WorkerBonusMap extends AbstractMap
{
    function __construct()
    {
        $this->buildDefaultMap();
        $this->ignoreMember('id');
        $this->setSkipNull(true);
    }

    /**
     * @return string The source type
     */
    public function getSourceType()
    {
        return WorkerBonus::class;
    }

    /**
     * @return string The destination type
     */
    public function getDestinationType()
    {
        return \GbsLogistics\Teamster\DocumentBundle\Document\WorkerBonus::class;
    }
}