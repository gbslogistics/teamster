<?php

namespace GbsLogistics\Teamster\DocumentBundle\Map;


use BCC\AutoMapperBundle\Mapper\AbstractMap;
use GbsLogistics\Doramad\Domain\SolarSystem;

class SolarSystemMap extends AbstractMap
{
    function __construct()
    {
        $this->buildDefaultMap();
        $this->ignoreMember('id');
    }

    /**
     * @return string The source type
     */
    public function getSourceType()
    {
        return SolarSystem::class;
    }

    /**
     * @return string The destination type
     */
    public function getDestinationType()
    {
        return \GbsLogistics\Teamster\DocumentBundle\Document\SolarSystem::class;
    }
}