<?php

namespace GbsLogistics\Teamster\DocumentBundle\Map;


use BCC\AutoMapperBundle\Mapper\AbstractMap;
use BCC\AutoMapperBundle\Mapper\FieldAccessor\Closure;
use BCC\AutoMapperBundle\Mapper\FieldAccessor\Simple;
use BCC\AutoMapperBundle\Mapper\Mapper;
use GbsLogistics\Doramad\Domain\IndustryTeam\CharacterBid;
use GbsLogistics\Teamster\DocumentBundle\Document\Character;

class CharacterBidMap extends AbstractMap
{
    /** @var Mapper */
    private $mapper;

    function __construct(Mapper $mapper)
    {
        $this->buildDefaultMap();
        $this->ignoreMember('id');
        $this->setSkipNull(true);

        $this->mapper = $mapper;
    }

    /**
     * @return string The source type
     */
    public function getSourceType()
    {
        return CharacterBid::class;
    }

    /**
     * @return string The destination type
     */
    public function getDestinationType()
    {
        return \GbsLogistics\Teamster\DocumentBundle\Document\CharacterBid::class;
    }

    public function buildDefaultMap()
    {
        parent::buildDefaultMap();

        $this->forMember('character', new Closure(function (CharacterBid $characterBid) {
            $character = new Character();
            $this->mapper->map($characterBid->getCharacter(), $character);
            return $character;
        }));

        return $this;
    }
}