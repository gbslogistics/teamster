<?php

namespace GbsLogistics\Teamster\DocumentBundle\Map;


use BCC\AutoMapperBundle\Mapper\AbstractMap;
use BCC\AutoMapperBundle\Mapper\FieldAccessor\Closure;
use BCC\AutoMapperBundle\Mapper\FieldAccessor\Simple;
use BCC\AutoMapperBundle\Mapper\Mapper;
use GbsLogistics\Doramad\Domain\IndustryTeam\CharacterBid;
use GbsLogistics\Doramad\Domain\IndustryTeam\SolarSystemBid;
use GbsLogistics\Teamster\DocumentBundle\Document\CharacterBid as DocumentBid;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystem;

class SolarSystemBidMap extends AbstractMap
{
    /** @var Mapper */
    private $mapper;

    function __construct(Mapper $mapper)
    {
        $this->buildDefaultMap();
        $this->ignoreMember('id');
        $this->setSkipNull(true);

        $this->mapper = $mapper;
    }

    /**
     * @return string The source type
     */
    public function getSourceType()
    {
        return SolarSystemBid::class;
    }

    /**
     * @return string The destination type
     */
    public function getDestinationType()
    {
        return \GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemBid::class;
    }

    public function buildDefaultMap()
    {
        parent::buildDefaultMap();

        $this->forMember('solarSystem', new Closure(
            function (SolarSystemBid $solarSystemBid) {
                $solarSystem = new SolarSystem();
                $this->mapper->map($solarSystemBid->getSolarSystem(), $solarSystem);
                return $solarSystem;
            }
        ));

        $this->forMember('characterBids', new Closure(function (SolarSystemBid $solarSystemBid) {
            $characterBids = [];
            /** @var CharacterBid $characterBid */
            foreach ($solarSystemBid->getCharacterBids() as $characterBid) {
                $documentBid = new DocumentBid();
                $this->mapper->map($characterBid, $documentBid);
                $characterBids[] = $documentBid;
            }

            return $characterBids;
        }));

        return $this;
    }

}