<?php

namespace GbsLogistics\Teamster\DocumentBundle\Map;
use BCC\AutoMapperBundle\Mapper\AbstractMap;
use BCC\AutoMapperBundle\Mapper\FieldAccessor\Closure;
use BCC\AutoMapperBundle\Mapper\Mapper;
use GbsLogistics\Doramad\Domain\IndustryTeam;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustryTeam as DocumentTeam;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystem;
use GbsLogistics\Teamster\DocumentBundle\Document\SolarSystemBid;
use GbsLogistics\Teamster\DocumentBundle\Document\Worker;
use GbsLogistics\Teamster\DocumentBundle\Map\FieldFilter\EmptyArrayIsNullFilter;


/**
 * @author Querns <querns@gbs.io>
 */
class IndustryTeamMap extends AbstractMap
{
    /** @var Mapper */
    private $mapper;

    function __construct(Mapper $mapper)
    {
        $this->buildDefaultMap();
        $this->ignoreMember('id');
        $this->setSkipNull(true);

        $this->mapper = $mapper;
    }

    /**
     * @return string The source type
     */
    public function getSourceType()
    {
        return IndustryTeam::class;
    }

    /**
     * @return string The destination type
     */
    public function getDestinationType()
    {
        return DocumentTeam::class;
    }

    public function buildDefaultMap()
    {
        parent::buildDefaultMap();

        $this->forMember('name', new Closure(
            function (IndustryTeam $team) {
                return str_replace('<br>', ' ', $team->getName());
            }
        ));

        $this->forMember('solarSystem', new Closure(
            function (IndustryTeam $industryTeam) {
                $solarSystem = new SolarSystem();
                $this->mapper->map($industryTeam->getSolarSystem(), $solarSystem);
                return $solarSystem;
            }
        ));

        $this->forMember('workers', new Closure(function (IndustryTeam $industryTeam) {
            $workers = [];
            /** @var IndustryTeam\Worker $worker */
            foreach ($industryTeam->getWorkers() as $worker) {
                $documentWorker = new Worker();
                $this->mapper->map($worker, $documentWorker);
                $workers[] = $documentWorker;
            }

            return $workers;
        }));

        $this->forMember('solarSystemBids', new Closure(function (IndustryTeam $industryTeam) {
            $bids = [];
            /** @var IndustryTeam\SolarSystemBid $solarSystemBid */
            foreach ($industryTeam->getSolarSystemBids() as $solarSystemBid) {
                $documentBid = new SolarSystemBid();
                $this->mapper->map($solarSystemBid, $documentBid);
                $bids[] = $documentBid;
            }

            return $bids;
        }));

        // This prevents the mapper from overwriting solar system bid info if
        // the source no longer has any information (in conjunction with the
        // $this->setSkipNull(true) call in the constructor.)
        $this->filter('solarSystemBids', new EmptyArrayIsNullFilter());

        return $this;
    }
}

