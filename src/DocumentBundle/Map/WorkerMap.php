<?php

namespace GbsLogistics\Teamster\DocumentBundle\Map;


use BCC\AutoMapperBundle\Mapper\AbstractMap;
use BCC\AutoMapperBundle\Mapper\FieldAccessor\Closure;
use BCC\AutoMapperBundle\Mapper\Mapper;
use GbsLogistics\Doramad\Domain\IndustryTeam\Worker;
use GbsLogistics\Teamster\DocumentBundle\Document\WorkerBonus;

class WorkerMap extends AbstractMap
{
    /** @var Mapper */
    private $mapper;

    function __construct(Mapper $mapper)
    {
        $this->buildDefaultMap();
        $this->ignoreMember('id');

        $this->mapper = $mapper;
    }

    /**
     * @return string The source type
     */
    public function getSourceType()
    {
        return Worker::class;
    }

    /**
     * @return string The destination type
     */
    public function getDestinationType()
    {
        return \GbsLogistics\Teamster\DocumentBundle\Document\Worker::class;
    }

    public function buildDefaultMap()
    {
        parent::buildDefaultMap();

        $this->forMember('bonus', new Closure(function (Worker $worker) {
            $bonus = new WorkerBonus();
            $this->mapper->map($worker->getBonus(), $bonus);
            return $bonus;
        }));

        return $this;
    }
}