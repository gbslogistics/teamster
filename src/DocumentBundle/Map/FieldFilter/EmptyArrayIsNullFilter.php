<?php

namespace GbsLogistics\Teamster\DocumentBundle\Map\FieldFilter;


use BCC\AutoMapperBundle\Mapper\FieldFilter\FieldFilterInterface;

class EmptyArrayIsNullFilter implements FieldFilterInterface
{
    /**
     * Applies the filter to a given value.
     *
     * @param $value mixed The value to filter
     * @return mixed The filtered value
     */
    function filter($value)
    {
        if (is_array($value) && count($value) == 0) {
            return null;
        }

        return $value;
    }
}