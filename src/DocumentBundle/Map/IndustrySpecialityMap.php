<?php

namespace GbsLogistics\Teamster\DocumentBundle\Map;
use BCC\AutoMapperBundle\Mapper\AbstractMap;
use GbsLogistics\Doramad\Domain\IndustrySpeciality;


/**
 * @author Querns <querns@gbs.io>
 */
class IndustrySpecialityMap extends AbstractMap
{
	function __construct()
	{
		$this->buildDefaultMap();
		$this->ignoreMember('id');
	}

	/**
	 * @return string The source type
	 */
	public function getSourceType()
	{
		return IndustrySpeciality::class;
	}

	/**
	 * @return string The destination type
	 */
	public function getDestinationType()
	{
		return \GbsLogistics\Teamster\DocumentBundle\Document\IndustrySpeciality::class;
	}
}