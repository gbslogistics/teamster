<?php

namespace GbsLogistics\Teamster\DocumentBundle\Map;


use BCC\AutoMapperBundle\Mapper\AbstractMap;
use GbsLogistics\Doramad\Domain\Character;

class CharacterMap extends AbstractMap
{
    function __construct()
    {
        $this->buildDefaultMap();
        $this->ignoreMember('id');
    }

    /**
     * @return string The source type
     */
    public function getSourceType()
    {
        return Character::class;
    }

    /**
     * @return string The destination type
     */
    public function getDestinationType()
    {
        return \GbsLogistics\Teamster\DocumentBundle\Document\Character::class;
    }
}