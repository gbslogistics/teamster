<?php

namespace GbsLogistics\Teamster\DocumentBundle\Map;


use BCC\AutoMapperBundle\Mapper\AbstractMap;
use GbsLogistics\Teamster\DocumentBundle\Document\ConquerableStation;
use Pheal\Core\RowSetRow;

class ConquerableStationMap extends AbstractMap
{
	function __construct()
	{
		$this->buildDefaultMap();
		$this->ignoreMember('id');
	}

	/**
	 * @return string The source type
	 */
	public function getSourceType()
	{
		return RowSetRow::class;
	}

	/**
	 * @return string The destination type
	 */
	public function getDestinationType()
	{
		return ConquerableStation::class;
	}
}