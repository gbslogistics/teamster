<?php

namespace GbsLogistics\Teamster\DocumentBundle\Map;


use BCC\AutoMapperBundle\Mapper\AbstractMap;
use GbsLogistics\Teamster\DocumentBundle\Document\CharacterAffiliation;
use Pheal\Core\RowSetRow;

class CharacterAffiliationMap extends AbstractMap
{
   	function __construct()
	{
		$this->buildDefaultMap();
	}

    /**
     * @return string The source type
     */
    public function getSourceType()
    {
        return RowSetRow::class;
    }

    /**
     * @return string The destination type
     */
    public function getDestinationType()
    {
        return CharacterAffiliation::class;
    }
}