<?php

namespace GbsLogistics\Teamster\FrontendBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustrySpeciality;
use GbsLogistics\Teamster\DocumentBundle\Document\IndustryTeam;
use GbsLogistics\Teamster\DocumentBundle\Document\Repository\IndustrySpecialityRepository;
use GbsLogistics\Teamster\DocumentBundle\Document\Repository\IndustryTeamRepository;
use GbsLogistics\Teamster\FrontendBundle\NumericIdParser;

class ApiController extends FOSRestController
{
    /** @var NumericIdParser */
    private $idParser;

    function __construct()
    {
        $this->idParser = new NumericIdParser();
    }

    public function getTeamAction($teamId)
    {
        $parsedIds = $this->idParser->parse($teamId);
        $viewObjects = null;

        if (null === $parsedIds) {
            return $this->return400('No team id was specified.');
        } elseif (is_array($parsedIds)) {
            $viewObjects = $this->getTeamRepository()->fetchByTeamIds($parsedIds);
        } else {
            $viewObjects = $this->getTeamRepository()->findOneBy(['teamId' => $parsedIds]);
        }

        if (empty($viewObjects)) {
            return $this->return404('Unable to find the specified team.');
        }
        return $this->handleView($this->view($viewObjects));
    }

    public function getSpecialityAction($specialtyId)
    {
        $parsedIds = $this->idParser->parse($specialtyId);

        if (null === $parsedIds) {
            return $this->return400('No industry specialization was specified.');
        } elseif (is_array($parsedIds)) {
            $viewObjects = $this->getSpecialityRepository()->fetchBySpecialtyIds($parsedIds);
        } else {
            $viewObjects = $this->getSpecialityRepository()->findOneBy(['specializationId' => $parsedIds]);
        }

        if (empty($viewObjects)) {
            return $this->return404('Unable to find the specified industry specialization.');
        }
        return $this->handleView($this->view($viewObjects));
    }

    /**
     * @return IndustryTeamRepository
     */
    private function getTeamRepository()
    {
        $documentRepository = $this->get('doctrine.odm.mongodb.document_manager')
            ->getRepository(IndustryTeam::class);
        return $documentRepository;
    }

    /**
     * @return IndustrySpecialityRepository
     */
    private function getSpecialityRepository()
    {
        $documentRepository = $this->get('doctrine.odm.mongodb.document_manager')
            ->getRepository(IndustrySpeciality::class);
        return $documentRepository;
    }

    /**
     * @param string $message
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function return404($message)
    {
        return $this->returnResponse([
            'message' => $message,
        ], 404);
    }

    private function return400($message)
    {
        return $this->returnResponse([
            'message' => $message,
        ], 400);
    }

    /**
     * @param mixed $data
     * @param int $statusCode
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function returnResponse($data, $statusCode)
    {
        return $this->handleView($this->view($data, $statusCode));
    }
}
