<?php

namespace GbsLogistics\Teamster\FrontendBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('GbsLogisticsTeamsterFrontendBundle::index.html.twig');
    }

    public function testAction()
    {
        return $this->render('GbsLogisticsTeamsterFrontendBundle::test.html.twig', [
            'testFiles' => $this->getTestFiles()
        ]);
    }

    /**
     * @return array
     */
    private function getTestFiles()
    {
        $files = [];
        $path = __DIR__ . '/../Resources/public/js/test/';

        if ($dir = opendir($path)) {
            while (false !== $file = readdir($dir)) {
                if (is_file($path . '/' . $file)) {
                    $files[] = $file;
                }
            }
        }
        closedir($dir);

        return $files;
    }
}
