<?php

namespace GbsLogistics\Teamster\FrontendBundle;


class NumericIdParser
{
    public function parse($idString)
    {
        $return = null;

        if (is_numeric($idString)) {
            $return = $this->transformStringId($idString);
        } elseif (is_string($idString) && false !== strpos($idString, ',')) {
            $ids = explode(',', $idString);
            $return = [];

            foreach ($ids as $id) {
                if (is_numeric($id)) {
                    $transformedId = $this->transformStringId($id);
                    $return[$transformedId] = $transformedId;
                }
            }

            $return = array_values($return);

            $count = count($return);
            if (1 === $count) {
                $return = $return[0];
            } elseif (0 === $count) {
                $return = null;
            }
        }

        return $return;
    }

    /**
     * @param $idString
     * @return int
     */
    private function transformStringId($idString)
    {
        $return = (int)$idString;
        return $return;
    }
}
