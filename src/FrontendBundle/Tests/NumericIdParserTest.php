<?php
namespace GbsLogistics\Teamster\FrontendBundle\Tests;

use GbsLogistics\Teamster\FrontendBundle\NumericIdParser;

class NumericIdParserTest extends \PHPUnit_Framework_TestCase
{
    /** @var NumericIdParser */
    private $sut;

    public function setUp()
    {
        $this->sut = new NumericIdParser();
    }

    /**
     * @dataProvider parseSingleIdProvider
     * @param $id
     * @param $expectedValue
     */
    public function testParseSingleId($id, $expectedValue)
    {
        $this->assertEquals($expectedValue, $this->sut->parse($id));
    }

    public function parseSingleIdProvider()
    {
        return [
            [ "1", 1 ],
            [ "123.0", 123 ],
            [ "string", null ],
            [ new \stdClass(), null ],
            [ null, null ],
        ];
    }

    /**
     * @dataProvider parseMultipleIdsProvider
     * @param $idString
     * @param $expectedValues
     */
    public function testParseMultipleIds($idString, $expectedValues)
    {
        $this->assertEquals($expectedValues, $this->sut->parse($idString));
    }

    public function parseMultipleIdsProvider() {
        return [
            [ "123,456", [123, 456] ],
            [ "string,123", 123 ],
            [ "123,123", 123 ],
            [ "123,456,123", [123, 456] ],
            [ null, null ],
        ];
    }
}
 