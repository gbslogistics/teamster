angular.module('teamsterFilters', [])
    .filter('teamsterCurrency', function () {
        "use strict";
        var formatter = new TwitterCldr.CurrencyFormatter();
        return function (input) {
            return formatter.format(input, { currency: " " });
        };
    })

    .filter('gbsDateFormat', function () {
        "use strict";
        return function (rawDate) {
            return new Date(rawDate).toUTCString();
        };
    })

    .filter('gbsPercentFormat', function () {
        "use strict";
        var formatter = new TwitterCldr.PercentFormatter();

        return function (rawPercent, precision) {
            var formattedPercent;
            if (undefined !== precision) {
                formattedPercent = formatter.format(rawPercent, { precision: precision });
            } else {
                formattedPercent = formatter.format(rawPercent);
            }

            return formattedPercent;
        };
    })

    .filter('gbsAbbreviateNumber', function () {
        "use strict";
        var formatter = new TwitterCldr.ShortDecimalFormatter();

        return function (input, precision) {
            var formattedNumber = "";
            if (undefined === precision) {
                formattedNumber = formatter.format(input);
            } else {
                formattedNumber = formatter.format(input, {precision: 2});
            }

            return formattedNumber;
        };
    })

    .filter('gbsNumberFormat', function () {
        "use strict";
        var formatter = new TwitterCldr.DecimalFormatter();

        return function (input, precision) {
            var formattedNumber;

            if (undefined === input) {
                formattedNumber = "";
            } else if (undefined === precision) {
                formattedNumber = formatter.format(input);
            } else {
                formattedNumber = formatter.format(input, { precision: precision });
            }

            return formattedNumber;
        };
    })

    .filter('gbsRelativeTime', function () {
        "use strict";
        var formatter = new TwitterCldr.TimespanFormatter();

        return function (input, options) {
            return formatter.format(input, options);
        };
    });

