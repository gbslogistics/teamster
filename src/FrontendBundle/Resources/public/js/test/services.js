describe("Data Manipulation Test Suite", function () {
    "use strict";
    var dataManipulationService;

    beforeEach(module('teamsterServices'));

    //noinspection JSLint
    beforeEach(inject(function (_dataManipulation_) {
        //noinspection JSLint
        dataManipulationService = _dataManipulation_;
    }));

    it("should have services defined", function () {
        expect(typeof dataManipulationService.numericArrayToSet).toEqual("function");
        expect(typeof dataManipulationService.idStringParser).toEqual("function");
    });

    describe("The numericArrayToSet service", function () {
        it("should return a set", function () {
            var set = dataManipulationService.numericArrayToSet();
            expect(set.constructor.name).toEqual("Set");
        });

        describe("when receiving a scalar value as an argument", function () {
            it("should accept a number and return a set with one value", function () {
                var set = dataManipulationService.numericArrayToSet(123);
                expect(set.size).toBe(1);
                expect(set.has(123)).toBeTruthy();
            });

            it("should accept a string that is a number and return a set with one value", function () {
                var set = dataManipulationService.numericArrayToSet("123");
                expect(set.size).toBe(1);
                expect(set.has(123)).toBeTruthy();
            });

            it("should throw if a non-numeric string is passed", function () {
                expect(function () {
                    dataManipulationService.numericArrayToSet("not a number");
                }).toThrow();
            });

            it("should return an empty set if nothing is provided as an argument", function () {
                var set = dataManipulationService.numericArrayToSet();
                expect(set.size).toBe(0);
            });

            it("should throw if an invalid variable type is provided as an argument", function () {
                expect(function () {
                    dataManipulationService.numericArrayToSet({});
                }).toThrow();
            });
        });

        describe("when receiving an array as an argument", function () {
            it("should process numeric values in an array", function () {
                var set = dataManipulationService.numericArrayToSet([123, 456]);
                expect(set.size).toBe(2);
                expect(set.has(123)).toBeTruthy();
                expect(set.has(456)).toBeTruthy();
            });

            it("should process numeric strings in an array", function () {
                var set = dataManipulationService.numericArrayToSet(["123", "456"]);
                expect(set.size).toBe(2);
                expect(set.has(123)).toBeTruthy();
                expect(set.has(456)).toBeTruthy();
            });

            it("should throw if a non-numeric string is the passed array", function () {
                expect(function () {
                    dataManipulationService.numericArrayToSet(["not a number"]);
                }).toThrow();
            });

            it("should return an empty set if passed an empty array", function () {
                var set = dataManipulationService.numericArrayToSet([]);
                expect(set.size).toBe(0);
            });

            it("should throw if an invalid variable type is in the passed array", function () {
                expect(function () {
                    dataManipulationService.numericArrayToSet([{}]);
                }).toThrow();
            });
        });
    });

    describe("The idStringParser service", function () {
        describe("when receiving a scalar value as an argument", function () {
            it("should accept a number and return a string", function () {
                var output = dataManipulationService.idStringParser(123);

                expect(typeof output).toBe("string");
                expect(output).toBe("123");
            });

            it("should accept a string and return that string", function () {
                var output = dataManipulationService.idStringParser("123");

                expect(typeof output).toBe("string");
                expect(output).toBe("123");
            });

            it("should throw when string is non-numeric", function () {
                expect(function () {
                    dataManipulationService.idStringParser("non-numeric string");
                }).toThrow();
            });

            it("should throw when receiving any other non-array types of input", function () {
                expect(function () {
                    dataManipulationService.idStringParser({});
                }).toThrow();
            });
        });
        describe("when receiving an array as an argument", function () {
            it("should return a comma-separated string when passed an array of numbers", function () {
                var output = dataManipulationService.idStringParser([123, 456]);

                expect(typeof output).toBe("string");
                expect(output).toBe("123,456");
            });

            it("should return a comma-separated string when passed an array of strings", function () {
                var output = dataManipulationService.idStringParser(["123", "456"]);

                expect(typeof output).toBe("string");
                expect(output).toBe("123,456");
            });

            it("should de-dupe the array", function () {
                var output = dataManipulationService.idStringParser(["123", 123, "456", 456, 123]);

                expect(typeof output).toBe("string");
                expect(output).toBe("123,456");
            });

            it("should throw when an array member is neither number nor string", function () {
                expect(function () {
                    dataManipulationService.idStringParser([{}]);
                }).toThrow();
            });

            it("should throw when a non-numeric string is in the input array", function () {
                expect(function () {
                    dataManipulationService.idStringParser(["non-numeric string"]);
                }).toThrow();
            });
        });
    });
});

