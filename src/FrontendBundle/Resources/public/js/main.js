var teamsterApp = angular.module('teamsterApp', [
    'ngRoute',
    'teamsterControllers',
    'teamsterDirectives',
    'teamsterFilters'
]);

teamsterApp.config([
    '$routeProvider',
    function ($routeProvider) {
        "use strict";
        $routeProvider.when('/team/:teamId', {
            templateUrl: '/partials/single-team.html',
            controller: 'SingleTeamCtrl'
        }).otherwise({
            redirectTo: '/'
        });
    }
]);
