angular.module('teamsterDirectives', [])
    .directive('teamsterSpecialization', [
        function () {
            "use strict";
            return {
                templateUrl: '/partials/specialization.html',
                scope: {
                    bonus: "&",
                    specialization: "&"
                }
            };
        }
    ])
    .directive('teamsterSolarSystem', [
        function () {
            "use strict";
            return {
                templateUrl: "/partials/solar-system.html",
                scope: {
                    name: "&",
                    geography: "&"
                },
                controller: function ($scope) {
                    $scope.systemType = function () {
                        if (undefined !== $scope.geography()) {
                            var geography = $scope.geography(),
                                security = geography.security,
                                factionId = geography.hasOwnProperty('factionId') ? geography.factionId : null,
                                wormholeClass = geography.wormholeClass,
                                systemType = "";

                            if (security >= 0.45) {
                                systemType = "highsec";
                            } else if (security < 0.45 && security >= 0.00) {
                                systemType = "lowsec";
                            } else {
                                if (undefined === wormholeClass) {
                                    if (factionId) {
                                        systemType = "npc 0.0";
                                    } else {
                                        systemType = "conquerable 0.0";
                                    }
                                } else {
                                    systemType = "wormhole";
                                }
                            }

                            return systemType;
                        }

                        return "";
                    };
                }
            };
        }
    ]);
