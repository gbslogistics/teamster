//noinspection SpellCheckingInspection
angular.module('teamsterControllers', [
    'teamsterServices'
])
    .controller('SingleTeamCtrl', ['$scope', '$routeParams', '$interval', 'teamFetcher',
        function ($scope, $routeParams, $interval, teamFetcher) {
            "use strict";
            var pulseInterval;

            teamFetcher(
                $routeParams.teamId,
                function (team, specializations) {
                    team.auctionExpiryMils = (new Date(team.auctionExpiryTime)).getTime();
                    team.createdMils = (new Date(team.createdTime)).getTime();
                    team.expiryMils = (new Date(team.expiryTime)).getTime();

                    $scope.team = team;
                    $scope.specializations = specializations;
                }
            );
            pulseInterval = $interval(function () {
                $scope.pulse = (new Date()).getTime();
            }, 1000);

            $scope.$on('$destroy', function () {
                $interval.cancel(pulseInterval);
            });
        }]);
