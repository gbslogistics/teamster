angular.module('teamsterServices', [
    'angular-data.DSCacheFactory'
])
    .factory('dataManipulation', [ function () {
        "use strict";
        var numericArrayToSet = function (rawInput) {
            var set = new Set();

            function processInput(input) {
                var number;

                switch (typeof input) {
                case "string":
                    number = parseInt(input, 10);
                    if (isNaN(number)) {
                        throw "A non-numeric string was passed as an argument to numericArrayToSet.";
                    }
                    break;
                case "number":
                    number = input;
                    break;
                default:
                    throw "All arguments passed to numericArrayToSet must be numbers or numeric strings.";
                }

                return number;
            }

            if (Array.isArray(rawInput)) {
                angular.forEach(rawInput, function (member) {
                    set.add(processInput(member));
                });
            } else if (undefined !== rawInput) {
                set.add(processInput(rawInput));
            }

            return set;
        }, idStringParser = function (ids) {
            var idString = "",
                set = numericArrayToSet(ids);

            set.forEach(function (item) {
                var itemAsString = item.toString();
                if (0 === idString.length) {
                    idString = itemAsString;
                } else {
                    idString = idString + "," + itemAsString;
                }
            });

            return idString;
        };

        return {
            numericArrayToSet: numericArrayToSet,
            idStringParser: idStringParser
        };
    }])

    .factory('teamFetcher', ['$http', 'DSCacheFactory', 'dataManipulation', 'specializationFetcher',
        function ($http, DSCacheFactory, dataManipulation, specializationFetcher) {
            "use strict";
            var teamFetchCache = 'SingleTeamCtrlFetchCache';

            //noinspection JSLint
            DSCacheFactory(teamFetchCache, {
                deleteOnExpire: "aggressive",
                // 15 minutes
                maxAge: 900000,
                // 1 minute
                recycleFreq: 60000
            });

            return function (dirtyIds, callback) {
                var idString = dataManipulation.idStringParser(dirtyIds),
                    url = Routing.generate("teamster_api_get_team", { teamId: idString, "_format": "json" }),
                    specializationIds = [];

                $http.get(url, { cache: DSCacheFactory.get(teamFetchCache) })
                    .success(function (team) {
                        specializationIds.push(team.specializationId);
                        //noinspection JSUnresolvedVariable
                        angular.forEach(team.workers, function (worker) {
                            //noinspection JSUnresolvedVariable
                            specializationIds.push(worker.specializationId);
                        });

                        specializationFetcher(specializationIds).then(function (specializations) {
                            callback(team, specializations);
                        }).catch(function (error) {
                            callback(team, error);
                        });
                    })
                    .error(function (data, status) {
                        callback({
                            error: "Error",
                            errorDescription: data.message + " (Error Code: " + status.toString() + ")"
                        });
                    });
            };
        }])

    .factory('specializationFetcher', ['$http', '$q', 'DSCacheFactory', 'dataManipulation',
        function ($http, $q, DSCacheFactory, dataManipulation) {
            "use strict";
            var specializationCache = 'SpecializationFetchCache',
                cache = new DSCacheFactory(specializationCache);

            return function (rawSpecializationIds) {
                var deferred = $q.defer(),
                    specializationIds = dataManipulation.numericArrayToSet(rawSpecializationIds),
                    specializations = [],
                    idsToLookup = [],
                    map = new Map(),
                    i = 0;

                function sortSpecializations(a, b) {
                    var aRank = map.get(a.specializationId),
                        bRank = map.get(b.specializationId);

                    return aRank - bRank;
                }

                specializationIds.forEach(function (id) {
                    var specialization = cache.get(id);

                    map.set(id, i);
                    i += 1;

                    if (undefined === specialization) {
                        idsToLookup.push(id);
                    } else {
                        specializations.push(specialization);
                    }
                });

                if (idsToLookup.length > 0) {
                    $http.get(Routing.generate("teamster_api_get_speciality", {
                        specialtyId: dataManipulation.idStringParser(idsToLookup),
                        "_format": "json"
                    })).success(function (data) {
                        angular.forEach(data, function (specialization) {
                            if (!specialization.hasOwnProperty('specializationId')
                                    || !specialization.hasOwnProperty('name')) {
                                deferred.reject("Received malformed specialization from AJAX lookup.");
                            }

                            //noinspection JSUnresolvedVariable
                            cache.put(specialization.specializationId, specialization);
                            specializations.push(specialization);
                        });

                        specializations.sort(sortSpecializations);
                        deferred.resolve(specializations);
                    }).error(function (data, status) {
                        deferred.reject({
                            error: "Error",
                            errorDescription: data.message + " (Status Code: " + status + ")"
                        });
                    });
                } else {
                    specializations.sort(sortSpecializations);
                    deferred.resolve(specializations);
                }

                return deferred.promise;
            };
        }]);
