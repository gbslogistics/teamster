<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Doctrine\Bundle\MongoDBBundle\DoctrineMongoDBBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new \GbsLogistics\Teamster\BackendBundle\GbsLogisticsTeamsterBackendBundle(),
            new \GbsLogistics\Teamster\DocumentBundle\GbsLogisticsTeamsterDocumentBundle(),
            new \GbsLogistics\SdeEntityBundle\GbsLogisticsSdeEntityBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new \GbsLogistics\Teamster\FrontendBundle\GbsLogisticsTeamsterFrontendBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }

//    public function getCacheDir()
//    {
//        if ($this->environment !== 'prod') {
//            return '/dev/shm/teamster/'.$this->environment.'/cache';
//        } else {
//            return parent::getCacheDir();
//        }
//    }
}
